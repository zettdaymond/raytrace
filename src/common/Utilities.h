#pragma once

#include <glm/vec3.hpp>
#include <glm/exponential.hpp>

float randomDouble();

glm::vec3 randomInUnitSphere();


template <typename T>
inline bool IsPowerOf2(T v)
{
    return v && !(v & (v - 1));
}

inline int32_t NearestNextPow2(int32_t v)
{
    v--;
    v |= v >> 1;    v |= v >> 2;
    v |= v >> 4;    v |= v >> 8;
    v |= v >> 16;
    return v+1;
}

inline float GammaCorrect(float value)
{
    if (value <= 0.0031308f)
    {
        return 12.92f * value;
    }
    return 1.055f * glm::pow(value, 1.f / 2.4f) - 0.055f;
}

inline float InverseGammaCorrect(float value)
{
    if (value <= 0.04045f)
    {
        return value * 1.f / 12.92f;
    }
    return glm::pow((value + 0.055f) * 1.f / 1.055f, 2.4f);
}
