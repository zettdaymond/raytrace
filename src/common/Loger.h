#pragma once

#include <string_view>

#include <spdlog/spdlog.h>
#if __cplusplus > 201703L
//#if (defined(__GNUC__) && (__GNUC___ > 8)) || (defined(__clang__) && (__clang_major__ > 9))
#   include <experimental/source_location>

namespace log_details {
using source_location = std::experimental::source_location;
}
#else

namespace log_details {
struct source_location
{
    constexpr static auto current()
    {
        return source_location();
    }

    constexpr auto file_name() const
    {
        return "";
    }
    constexpr auto line() const
    {
        return "";
    }

    constexpr auto function_name() const
    {
        return "";
    }
};
}

#endif

namespace log_details
{

/**
 * Wrapper over user-provided formatter string that allows
 * grab the source information, like filename, column etc...
 */
template<class T>
class helper
{
public:
    constexpr helper(const T* str,const source_location sl = source_location::current()) noexcept :
        m_str(str),
        m_sl(sl)
    {}

    const T* m_str;
    const source_location m_sl;
};


constexpr std::string_view CutFilename(const std::string_view path)
{
    return path.substr(path.find_last_of('/') + 1);
}

template<class T, class ...Args>
constexpr auto GetExtentedString(const T& str, const log_details::source_location& sl, Args&&... args)
{
    const auto baseMessage = fmt::format(str, std::forward<Args>(args)...);
    auto filename = CutFilename(sl.file_name());
    return fmt::format("{} ({}:{} {})",
                       baseMessage,
                       filename.data(),
                       sl.line(),
                       sl.function_name());
}
}

class Loger
{
public:
    static void InitConsoleLoging();
};

template<class ...Args>
inline void LOGI(log_details::helper<char> message, Args&&... args)
{
    spdlog::info(log_details::GetExtentedString(message.m_str,
                                                message.m_sl,
                                                std::forward<Args>(args)...));
}

template<class ...Args>
inline void LOGD(log_details::helper<char> message, Args&&... args)
{
    spdlog::debug(log_details::GetExtentedString(message.m_str,
                                                 message.m_sl,
                                                 std::forward<Args>(args)...));
}

template<class ...Args>
inline void LOGE(log_details::helper<char> message, Args&&... args)
{
    spdlog::error(log_details::GetExtentedString(message.m_str,
                                                 message.m_sl,
                                                 std::forward<Args>(args)...));
}

template<class ...Args>
inline void LOGW(log_details::helper<char> message, Args&&... args)
{
    spdlog::warn(log_details::GetExtentedString(message.m_str,
                                                message.m_sl,
                                                std::forward<Args>(args)...));
}
