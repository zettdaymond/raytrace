#pragma once

#include "glm/vec3.hpp"
#include "glm/geometric.hpp"

inline uint8_t MaxDimension(const glm::vec3& v)
{
    return (v.x > v.y) ? ((v.x > v.z) ? 0 : 2) :
           ((v.y > v.z) ? 1 : 2);
}

template<class Integral>
inline glm::vec3 Permute(const glm::vec3& v, Integral x, Integral y, Integral z)
{
    static_assert (std::is_integral_v<Integral>);
    return glm::vec3(v[x], v[y], v[z]);
}

inline void PermuteInplace( glm::vec3& v, uint8_t x, uint8_t y, uint8_t z )
{
    std::swap( v.x, v[ x ] );
    std::swap( v.y, v[ y ] );
}

void MakeCoordinateSystem(const glm::vec3& v1, glm::vec3& v2, glm::vec3& v3)
{
    if (std::abs(v1.x) > std::abs(v1.y))
    {
        v2 = glm::vec3(-v1.z, 0, v1.x) / glm::sqrt(v1.x * v1.x + v1.z * v1.z);
    }
    else
    {
        v2 = glm::vec3(0, v1.z, -v1.y) / glm::sqrt(v1.y * v1.y + v1.z * v1.z);
    }
    v3 = glm::cross(v1, v2);
}
