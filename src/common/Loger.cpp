#include "Loger.h"

#include <spdlog/sinks/ansicolor_sink.h>
#include "spdlog/sinks/stdout_color_sinks.h"

namespace SLoger
{
bool IsTerminalSupportsASCIIColors()
{
    static constexpr std::array<const char *, 14> Terms = {
        {"ansi", "color", "console", "cygwin", "gnome", "konsole", "kterm", "linux", "msys", "putty", "rxvt", "screen", "vt100", "xterm"}};

    const char *env_p = std::getenv("TERM");
    if (env_p == nullptr)
    {
        return false;
    }

    static const bool result =
        std::any_of(std::begin(Terms), std::end(Terms), [&](const char *term) { return std::strstr(env_p, term) != nullptr; });
    return result;
}

}

void Loger::InitConsoleLoging()
{
    static bool isInitialized = false;
    if(isInitialized == false)
    {
        spdlog::set_pattern("%^[%T][%L]%$ %v");

        std::shared_ptr<spdlog::logger> loger = nullptr;
        if(SLoger::IsTerminalSupportsASCIIColors())
        {
            loger = spdlog::synchronous_factory::create<
                        spdlog::sinks::ansicolor_stdout_sink_mt>("console",spdlog::color_mode::always);
        }
        else //default platform log
        {
            loger = spdlog::stdout_color_mt("console");
        }
        spdlog::set_default_logger(loger);
        isInitialized = true;
    }
}

#if defined(WIN32)
#   include "spdlog/sinks/ansicolor_sink-inl.h"
    template class spdlog::sinks::ansicolor_sink<spdlog::details::console_mutex>;
    template class spdlog::sinks::ansicolor_sink<spdlog::details::console_nullmutex>;
    template class spdlog::sinks::ansicolor_stdout_sink<spdlog::details::console_mutex>;
    template class spdlog::sinks::ansicolor_stdout_sink<spdlog::details::console_nullmutex>;
    template class spdlog::sinks::ansicolor_stderr_sink<spdlog::details::console_mutex>;
    template class spdlog::sinks::ansicolor_stderr_sink<spdlog::details::console_nullmutex>;
#endif
