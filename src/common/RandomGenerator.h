#pragma once

#include <random>

#include "thirdparty/pcg-cpp/include/pcg_random.hpp"

class RandomGenerator
{
public:
    RandomGenerator()
        : realDistribution(0.f,1.f)
        , uintDistribution(0, std::numeric_limits<uint32_t>::max())
    {}

    uint32_t UniformUInt()
    {
        return uintDistribution(rng);
    }

    uint32_t UniformUint(uint32_t threshold)
    {
        return UniformUInt() % threshold;
    }

    float UniformFloat()
    {
        return realDistribution(rng);
    }

    void SetSeed(uint32_t newSeed)
    {
        rng.seed(newSeed);
    }

    pcg32::result_type operator()(pcg32::result_type val)
    {
        return rng(val);
    }

    pcg32::result_type operator()()
    {
        return rng();
    }

private:
    pcg32 rng;
    std::uniform_real_distribution<float> realDistribution;
    std::uniform_int_distribution<std::uint32_t> uintDistribution;
};
