#pragma once

#include <functional>

/** Wrapper around an enum that allows simple use of bitwise logic operations. */
template<typename Enum, typename Storage = std::underlying_type_t<Enum>>
class Flags
{
public:
    using InternalType = Storage;

    constexpr Flags() = default;

    constexpr Flags(Enum value)
    {
        mBits = static_cast<Storage>(value);
    }

    constexpr Flags(std::initializer_list<Enum> values)
    {
        for(const auto& v : values)
        {
            mBits &= static_cast<Storage>(v);
        }
    }

    constexpr Flags(const Flags<Enum, Storage>& value)
    {
        mBits = value.mBits;
    }

    constexpr explicit Flags(Storage bits)
    {
        mBits = bits;
    }

    /** Checks whether all of the provided bits are set */
    constexpr bool isSet(Enum value) const
    {
        return (mBits & static_cast<Storage>(value)) == static_cast<Storage>(value);
    }

    /** Checks whether any of the provided bits are set */
    constexpr bool isSetAny(Enum value) const
    {
        return (mBits & static_cast<Storage>(value)) != 0;
    }

    /** Checks whether any of the provided bits are set */
    constexpr bool isSetAny(const Flags<Enum, Storage>& value) const
    {
        return (mBits & value.mBits) != 0;
    }

    /** Activates all of the provided bits. */
    constexpr Flags<Enum, Storage>& set(Enum value)
    {
        mBits |= static_cast<Storage>(value);

        return *this;
    }

    /** Deactivates all of the provided bits. */
    constexpr void unset(Enum value)
    {
        mBits &= ~static_cast<Storage>(value);
    }

    constexpr bool operator==(Enum rhs) const
    {
        return mBits == static_cast<Storage>(rhs);
    }

    constexpr bool operator==(const Flags<Enum, Storage>& rhs) const
    {
        return mBits == rhs.mBits;
    }

    constexpr bool operator==(bool rhs) const
    {
        return ((bool)*this) == rhs;
    }

    constexpr bool operator!=(Enum rhs) const
    {
        return mBits != static_cast<Storage>(rhs);
    }

    constexpr bool operator!=(const Flags<Enum, Storage>& rhs) const
    {
        return mBits != rhs.mBits;
    }

    constexpr Flags<Enum, Storage>& operator= (Enum rhs)
    {
        mBits = static_cast<Storage>(rhs);

        return *this;
    }

    constexpr Flags<Enum, Storage>& operator= (const Flags<Enum, Storage>& rhs)
    {
        mBits = rhs.mBits;

        return *this;
    }

    constexpr Flags<Enum, Storage>& operator|= (Enum rhs)
    {
        mBits |= static_cast<Storage>(rhs);

        return *this;
    }

    constexpr Flags<Enum, Storage>& operator|= (const Flags<Enum, Storage>& rhs)
    {
        mBits |= rhs.mBits;

        return *this;
    }

    constexpr Flags<Enum, Storage> operator| (Enum rhs) const
    {
        Flags<Enum, Storage> out(*this);
        out |= rhs;

        return out;
    }

    constexpr Flags<Enum, Storage> operator| (const Flags<Enum, Storage>& rhs) const
    {
        Flags<Enum, Storage> out(*this);
        out |= rhs;

        return out;
    }

    constexpr Flags<Enum, Storage>& operator&= (Enum rhs)
    {
        mBits &= static_cast<Storage>(rhs);

        return *this;
    }

    constexpr Flags<Enum, Storage>& operator&= (const Flags<Enum, Storage>& rhs)
    {
        mBits &= rhs.mBits;

        return *this;
    }

    constexpr Flags<Enum, Storage> operator& (Enum rhs) const
    {
        Flags<Enum, Storage> out = *this;
        out.mBits &= static_cast<Storage>(rhs);

        return out;
    }

    constexpr Flags<Enum, Storage> operator& (const Flags<Enum, Storage>& rhs) const
    {
        Flags<Enum, Storage> out = *this;
        out.mBits &= rhs.mBits;

        return out;
    }

    constexpr Flags<Enum, Storage>& operator^= (Enum rhs)
    {
        mBits ^= static_cast<Storage>(rhs);

        return *this;
    }

    constexpr Flags<Enum, Storage>& operator^= (const Flags<Enum, Storage>& rhs)
    {
        mBits ^= rhs.mBits;

        return *this;
    }

    constexpr Flags<Enum, Storage> operator^ (Enum rhs) const
    {
        Flags<Enum, Storage> out = *this;
        out.mBits ^= static_cast<Storage>(rhs);

        return out;
    }

    constexpr Flags<Enum, Storage> operator^ (const Flags<Enum, Storage>& rhs) const
    {
        Flags<Enum, Storage> out = *this;
        out.mBits ^= rhs.mBits;

        return out;
    }

    constexpr Flags<Enum, Storage> operator~ () const
    {
        Flags<Enum, Storage> out;
        out.mBits = static_cast<Storage>(~mBits);

        return out;
    }

    constexpr operator bool() const
    {
        return mBits ? true : false;
    }

    constexpr operator uint8_t() const
    {
        return static_cast<uint8_t>(mBits);
    }

    constexpr operator uint16_t() const
    {
        return static_cast<uint16_t>(mBits);
    }

    constexpr operator uint32_t() const
    {
        return static_cast<uint32_t>(mBits);
    }

    constexpr friend Flags<Enum, Storage> operator&(Enum a, Flags<Enum, Storage> &b)
    {
        Flags<Enum, Storage> out;
        out.mBits = a & b.mBits;
        return out;
    }

private:
    InternalType mBits{0};
};

    /** Defines global operators for a Flags<Enum, Storage> implementation. */
#define BS_FLAGS_OPERATORS(Enum) BS_FLAGS_OPERATORS_EXT(Enum, UINT32)

    /** Defines global operators for a Flags<Enum, Storage> implementation. */
#define BS_FLAGS_OPERATORS_EXT(Enum, Storage)																   \
		inline Flags<Enum, Storage> operator|(Enum a, Enum b) { Flags<Enum, Storage> r(a); r |= b; return r; } \
		inline Flags<Enum, Storage> operator&(Enum a, Enum b) { Flags<Enum, Storage> r(a); r &= b; return r; } \
		inline Flags<Enum, Storage> operator~(Enum a) { return ~Flags<Enum, Storage>(a); }

    /** @cond SPECIALIZATIONS */



namespace std
{
/** Hash value generator for Flags<Enum, Storage>. */
template<class Enum, class Storage>
struct hash<Flags<Enum, Storage>>
{
    size_t operator()(const Flags<Enum, Storage>& key) const
    {
        return (Storage)key;
    }
};
}
