#include "Utilities.h"

#include <thread>
#include <random>
#include "glm/gtx/norm.hpp"

#include "thirdparty/pcg-cpp/include/pcg_random.hpp"

float randomDouble()
{
    static thread_local pcg32 generator(std::hash<std::thread::id>{}(std::this_thread::get_id()));

    //    static thread_local std::mt19937 generator;
    std::uniform_real_distribution<float> distribution(0.0, 1.0);
    return distribution(generator);
}

glm::vec3 randomInUnitSphere()
{
    glm::vec3 p;
    do {
        p = 2.0f * glm::vec3(randomDouble(), randomDouble(), randomDouble()) - glm::vec3(1,1,1);
    } while (glm::length2(p) >= 1.0);
    return p;
}
