#pragma once

#include <type_traits>
#include <cstddef>
#include <utility>

template <typename T, std::size_t Size>
class Pimpl
{
    static constexpr std::size_t alignment = std::alignment_of<std::max_align_t>::value;
    using Storage = typename std::aligned_storage<Size, alignment>::type;

public:
    template <typename... Args>
    Pimpl(Args&&... args)
    {
    	static_assert(sizeof(T) <= Size, "please increase size parameter on pimpl");
        new (&data) T(std::forward<Args>(args)...);
    }

    Pimpl(Pimpl const& o)
    {
    	static_assert(sizeof(T) <= Size, "please increase size parameter on pimpl");
        new (&data) T(*o);
    }

    Pimpl(Pimpl&  o)
    {
    	static_assert(sizeof(T) <= Size, "please increase size parameter on pimpl");
        new (&data) T(*o);
    }

    Pimpl(Pimpl&& o)
    {
    	static_assert(sizeof(T) <= Size, "please increase size parameter on pimpl");
        new (&data) T(std::move(*o));
    }

    ~Pimpl()
    {
        reinterpret_cast<T*>(&data)->~T();
    }

    Pimpl& operator=(Pimpl const& o)
    { 
        **this = *o; return *this; 
    }
    
    Pimpl& operator=(Pimpl&& o)
    { 
        **this = std::move(*o); return *this; 
    }

    T& operator*()
    { 
        return *reinterpret_cast<T*>(&data); 
    }
    
    T const& operator*()  const 
    { 
        return *reinterpret_cast<T const*>(&data); 
    }
    
    T* operator->()       
    { 
        return &**this; 
    }
    
    T const* operator->() const 
    { 
        return &**this;
    }

    friend inline void swap(Pimpl& lhs, Pimpl& rhs)
    { 
        using std::swap; 
        swap(*lhs, *rhs);
    }

private:
    Storage data;
};
