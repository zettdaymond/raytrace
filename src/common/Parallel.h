#pragma once

#include <vector>
#include <queue>
#include <thread>
#include <functional>
#include <mutex>
#include <condition_variable>

class ThreadPool
{
public:
    ThreadPool(std::size_t aNumThreads)
        : numThreads(aNumThreads)
    {
        for(std::size_t i = 0; i < numThreads; i++)
        {
            workers.emplace_back(
               std::thread(
                   [this]
                   {
                        while(true)
                        {
                            std::function<void()> task;

                            {
                                std::unique_lock<std::mutex> lock(queueLock);
                                condition.wait(lock,
                                               [this]{ return stop || !tasks.empty(); });

                                if(stop && tasks.empty())
                                {
                                    return;
                                }

                                task = std::move(tasks.front());

                                tasks.pop();
                            }

                            task();
                        }
                   }
               )
            );
        }
    }

    ~ThreadPool()
    {
        {
            std::unique_lock<std::mutex> lock(queueLock);
            stop = true;
        }

        condition.notify_all();

        for(std::thread &worker: workers)
        {
            worker.join();
        }
    }

    template<class Func, class... Args>
    void submitWork(Func&& function, Args... args)
    {
        auto f = std::bind(std::forward<Func>(function), std::forward<Args>(args)...);

        {
            std::unique_lock<std::mutex> lock(queueLock);

            if(stop)
            {
                return; // do not inplace
            }
            tasks.emplace(f);
        }

        condition.notify_one();
    }

private:
    std::vector< std::thread > workers;
    std::queue< std::function<void(void)> > tasks;

    const std::size_t numThreads;

    std::mutex queueLock;
    std::condition_variable condition;
    bool stop = false;
};


template<class Func, class Container>
void parallel_for(std::size_t numThreads, const Container& c, const Func& function)
{
	ThreadPool pool(numThreads);
	for(const auto& v : c)
	{
		pool.submitWork(function, v);
	}
}
