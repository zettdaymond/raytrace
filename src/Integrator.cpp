#include "Integrator.h"

#include "glm/vec2.hpp"

#include "Camera.h"
#include "Film.h"
#include "Primitive.h"
#include "common/Parallel.h"
#include "common/Utilities.h"
#include "common/Utilities.h"
#include "light/Light.h"
#include "material/Material.h"
#include "sampler/Sampler.h"
#include "scene/Scene.h"


namespace SSamplerIntegrator
{
    std::vector<Bounds2i> makeTileBounds(const glm::ivec2& filmSize,
                                         const glm::ivec2& tilesCount,
                                         const int tileSize)
    {
        std::vector<Bounds2i> output;

        for(int i = 0; i < tilesCount.x; i++)
        {
            for(int j = 0; j < tilesCount.y; j++)
            {
                glm::ivec2 startTile(0.0);

                int x0 = startTile.x + i * tileSize;
                int x1 = std::min(x0 + tileSize, filmSize.x);
                int y0 = startTile.y + j * tileSize;
                int y1 = std::min(y0 + tileSize, filmSize.y);

                Bounds2i tileBounds({x0, y0}, {x1, y1});
                output.push_back(std::move(tileBounds));
            }
        }

        return output;
    }
}

glm::vec3 getBackgroundColor(const Ray& ray)
{
    float t = 0.5f * (ray.direction.y + 1.f);

    static constexpr glm::vec3 whiteColor = glm::vec3(1.0, 1.0, 1.0);
    static constexpr glm::vec3 blueColor = glm::vec3(1.0, 1.0, 1.0);

    return glm::lerp(whiteColor, blueColor, t);
}

glm::vec3 gammaCorrection(const glm::vec3& color)
{
    return glm::vec3(GammaCorrect(color.x),
                     GammaCorrect(color.y),
                     GammaCorrect(color.z));
}

Integrator::Integrator(const std::shared_ptr<Camera> &aCamera,
                       const std::shared_ptr<Sampler> &aSampler)
    : camera(aCamera)
    , sampler(aSampler)
{}

void Integrator::Render(Scene &shape)
{
    using namespace SSamplerIntegrator;

    auto filmSize = camera->GetFilm().size;

    constexpr int tileSize = 16;
    glm::ivec2 tilesCount((filmSize.x + tileSize - 1) / tileSize,
                          (filmSize.y + tileSize - 1) / tileSize);

    std::vector<Bounds2i> tilesBounds = makeTileBounds(filmSize, tilesCount, tileSize);

//    const auto threadCount = 1;
    const auto threadCount = std::thread::hardware_concurrency();
    parallel_for(threadCount,
                 tilesBounds,
                 [this, &shape](const auto& tileBounds) -> void
                 {
                    const auto hashedThreadId = std::hash<std::thread::id>{}(std::this_thread::get_id());
                    auto threadLocalSampler = sampler->Clone(hashedThreadId);
                    Raytrace(*camera, tileBounds, shape, *threadLocalSampler);
                 });
}

void Integrator::Raytrace(Camera &camera, const Bounds2i &tile, Scene &world, Sampler &sampler)
{
    Film& buffer = camera.GetFilm();

    for(int j = tile.pMin.y; j < tile.pMax.y; j++)
    {
        for(int i = tile.pMin.x; i < tile.pMax.x; i++)
        {
            glm::vec3 color(0.f, 0.f, 0.f);

            glm::ivec2 pixel{i,j};

            sampler.StartPixel(pixel);
            do
            {
                glm::vec2 uv = sampler.GetUVCameraSample(glm::ivec2{i,j});
                Ray ray = camera.GetRay(uv);

                color += ComputeColor(ray, world, 0);
            }
            while(sampler.StartNextSample());

            color /= float(sampler.GetSamplesPerPixel());
            color = gammaCorrection(color);

            buffer(i, j) = color;
        }
    }
}


PathTracerIntegrator::PathTracerIntegrator(const std::shared_ptr<Camera> &aCamera,
                                           const std::shared_ptr<Sampler> &aSampler,
                                           int32_t aMaxDepth)
    : Integrator(aCamera, aSampler)
    , maxDepth(aMaxDepth)
{}

glm::vec3 PathTracerIntegrator::ComputeColor(const Ray &ray, Scene &scene, int32_t depth)
{
    glm::vec3 output;

    auto sceneIntersection = scene.hit(ray, 0.001f, std::numeric_limits<float>::max());

    if(sceneIntersection)
    {
        for(const auto& light : scene.lights)
        {
            auto sample =  light->SampleLight(*sceneIntersection);
            if(sample.visTester.isVisible(scene))
            {
                output += sample.radiance;
            }
        }

        if(depth > maxDepth)
        {
            return glm::vec3(0.f);
        }

        if( const auto scatter = sceneIntersection->primitive->scatter(ray, *sceneIntersection) )
        {
            return scatter->attenuation * ComputeColor(scatter->scatteredRay, scene,depth+1);
        }
        else
        {
            return glm::vec3(0.f); // all rays absorbed
        }
    }

    return getBackgroundColor(ray);
}

DirectLightIntegrator::DirectLightIntegrator(const std::shared_ptr<Camera> &aCamera,
                                             const std::shared_ptr<Sampler> &aSampler,
                                             int32_t aMaxDepth)
    : Integrator(aCamera, aSampler)
    , maxDepth(aMaxDepth)
{}

//glm::vec3 SampleBackground(Ray& ray, HitRecord& hit, std::int32_t numSamples)
//{
//    glm::vec3 out;
//    for(int i =0; i < numSamples; i++)
//    {
//        if( auto scatter = hit.primitive->scatter(ray, hit))
//        {
//            if(auto& backgroundHit = backgroundBox.hit(scatter->scatteredRay, 0.001f, std::numeric_limits<float>::max()))
//            {
//                out += scatter->attenuation * getBackgroundColor(scatter->scatteredRay);
//            }
//        }
//    }

//    out /= numSamples;

//    return out;
//}

glm::vec3 DirectLightIntegrator::ComputeColor(const Ray &ray, Scene &hittable, int32_t depth)
{
    //We assume that background is aur light source

    glm::vec3 output(0.0f);

    if(auto hit = hittable.hit(ray, 0.001f, std::numeric_limits<float>::max()))
    {
        //Sample light source, i.e background
        //output += SampleBackground(*hit, *sampler);

        if(depth < maxDepth)
        {
            //compute specular reflection
            //output += SpecularReflect();
        }

        return output;
    }

    return getBackgroundColor(ray);
}
