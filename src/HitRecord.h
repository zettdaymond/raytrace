#pragma once

#include "glm/vec3.hpp"
#include "glm/vec2.hpp"

class Primitive;

struct HitRecord
{
    struct Shading
    {
        glm::vec3 normal;
    };

    float t;
    glm::vec3 point;
    glm::vec3 normal;
    glm::vec2 uv;
    Primitive* primitive = nullptr;

    Shading shading;
};
