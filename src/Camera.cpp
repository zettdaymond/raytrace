#include "Camera.h"

#include <optional>

#include "glm/vec3.hpp"
#include "glm/geometric.hpp"
#include "glm/gtx/compatibility.hpp"
#include "glm/gtx/norm.hpp"

#include "Film.h"

Camera::Camera(const std::shared_ptr<Film> &aFilm, const Bounds2f &aScreenSpaceWindow)
    : film(aFilm)
    , screenSpaceWindow(aScreenSpaceWindow)
{}

glm::vec3 Camera::ToScreenSpace(const glm::vec3 &rasterSpace) const
{
    const auto ndcSpace = rasterSpace / glm::vec3(film->size, 1.0f);

    const auto windowScale = glm::vec3(screenSpaceWindow.pMax.x - screenSpaceWindow.pMin.x,
                                       screenSpaceWindow.pMin.y - screenSpaceWindow.pMax.y,
                                       1.0f);

    const auto translatedScreenSpace = ndcSpace * windowScale;

    const auto screenSpace = translatedScreenSpace + glm::vec3(screenSpaceWindow.pMin.x,
                                                               screenSpaceWindow.pMax.y,
                                                               0.0f);
    return screenSpace;
}

Bounds2f Camera::MakeDefaultScreenSpaceWindow() const
{
    Bounds2f screenWindow;

    float aspectRatio = float(film->size.x) / float(film->size.y);

    if (aspectRatio > 1.f) {
        screenWindow.pMin.x = -aspectRatio;
        screenWindow.pMax.x = aspectRatio;
        screenWindow.pMin.y = -1.f;
        screenWindow.pMax.y = 1.f;
    }
    else
    {
        screenWindow.pMin.x = -1.f;
        screenWindow.pMax.x = 1.f;
        screenWindow.pMin.y = -1.f / aspectRatio;
        screenWindow.pMax.y = 1.f / aspectRatio;
    }

    return screenWindow;
}

Transform Camera::GetTransform()
{
    return cameraToWorld;
}

void Camera::SetTransform(const Transform &tr)
{
    cameraToWorld = tr;
}

Film &Camera::GetFilm()
{
    return *film;
}

OrthoCamera::OrthoCamera(const std::shared_ptr<Film> &aFilm, float zNear, float zFar, float verticalSize)
    : Camera(aFilm, MakeOrthoProjection(verticalSize, aFilm->size))
{
    cameraToScreen = Scale(glm::vec3(1.f, 1.f, 1.f/(zFar - zNear))) *
            Translate(glm::vec3(0.f,0.f, zNear));

    screenToCamera = glm::inverse(cameraToScreen);
}

Ray OrthoCamera::GetRay(const glm::vec2 &pointOnFilm) const
{
    const glm::vec3 pointInScreenSpace = ToScreenSpace(glm::vec3(pointOnFilm, 0.0f));

    //Unproject to camera space
    const glm::vec3 pointInCameraSpace = screenToCamera * glm::vec4(pointInScreenSpace, 1.0f);

    const glm::vec3 direction(0.0f, 0.0f, -1.0f);

    const glm::vec3 originInWorldSpace = cameraToWorld(glm::vec4(pointInCameraSpace, 1.0f));
    const glm::vec3 destanationInWorldSpace = cameraToWorld(glm::vec4(pointInCameraSpace + direction, 1.0f));

    Ray ray(originInWorldSpace, glm::normalize(destanationInWorldSpace - originInWorldSpace));

    return ray;
}

Bounds2f OrthoCamera::MakeOrthoProjection(float verticalSize, glm::vec2 imageResolution) const
{
    float distanceToVerticalBorder = verticalSize / 2.0f;

    float aspectRatio = float(imageResolution.x) / float(imageResolution.y);

    return Bounds2f(glm::vec2(-distanceToVerticalBorder * aspectRatio, -distanceToVerticalBorder),
                    glm::vec2(distanceToVerticalBorder * aspectRatio, distanceToVerticalBorder));

}

PerspectiveCamera::PerspectiveCamera(const std::shared_ptr<Film> &aFilm, float aFieldOfView, float zNear, float zFar)
    : Camera(aFilm, MakeScreenBounds(aFieldOfView, zNear, aFilm->size))
{
    float aspectRatio = float(aFilm->size.x) / float(aFilm->size.y);

    cameraToScreen = glm::perspective(glm::radians(aFieldOfView), aspectRatio, zNear, zFar);

    screenToCamera = glm::inverse(cameraToScreen);
}

Ray PerspectiveCamera::GetRay(const glm::vec2 &pointOnFilm) const
{
    glm::vec2 ndc = pointOnFilm / glm::vec2(film->size) * 2.0f;
    ndc.x = ndc.x - 1;
    ndc.y = 1 - ndc.y;

    //Unproject to camera space
    const glm::vec4 pointInCameraSpace = screenToCamera * glm::vec4(ndc, 1.0f, 1.0f);

    const glm::vec3 rayOrigin(0.0, 0.0, 0.0);
    const glm::vec3 direction = glm::normalize(pointInCameraSpace /* - rayOrigin*/);

    const glm::vec3 originInWorldSpace = cameraToWorld(glm::vec4(rayOrigin, 1.0f));
    const glm::vec3 destanationInWorldSpace = cameraToWorld(glm::vec4(/*rayOrigin + 1.0f x */ direction, 1.0f));

    Ray ray(originInWorldSpace, glm::normalize(destanationInWorldSpace - originInWorldSpace));

    return ray;
}

Bounds2f PerspectiveCamera::MakeScreenBounds(float aFieldOfView, float zNear, glm::vec2 imageResolution)
{
    float halfFov = aFieldOfView / 2.0f;
    float halfY = glm::tan(glm::radians(halfFov));

    float aspectRatio = float(imageResolution.x) / float(imageResolution.y);

    Bounds2f out;
    out.pMin.y = -halfY;
    out.pMax.y = halfY;

    out.pMin.x = -halfY * aspectRatio;
    out.pMax.x = halfY * aspectRatio;

    return out;
}
