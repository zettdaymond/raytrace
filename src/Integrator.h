#pragma once

#include <memory>

#include "Ray.h"
#include "Bounds.h"

class Shape;
class Camera;
class Sampler;
class Primitive;
class Scene;

class Integrator
{
public:
    Integrator(const std::shared_ptr<Camera>& aCamera,
               const std::shared_ptr<Sampler>& aSampler);
	virtual ~Integrator() = default;

    void Render(Scene &shape);
    void Raytrace(Camera& camera, const Bounds2i &tile, Scene& world, Sampler& sampler);
    virtual glm::vec3 ComputeColor(const Ray& ray, Scene& hittable, std::int32_t depth = 0) = 0;

protected:
    std::shared_ptr<Camera> camera;
    std::shared_ptr<Sampler> sampler;
};


class PathTracerIntegrator final
        : public Integrator
{
public:
    PathTracerIntegrator(const std::shared_ptr<Camera>& aCamera,
                         const std::shared_ptr<Sampler>& aSampler,
                         int32_t aMaxDepth = 10);

    glm::vec3 ComputeColor(const Ray& ray, Scene& scene, std::int32_t depth = 0) override;

private:
    const std::int32_t maxDepth;
};


class DirectLightIntegrator final
        : public Integrator
{
public:
    DirectLightIntegrator(const std::shared_ptr<Camera>& aCamera,
                          const std::shared_ptr<Sampler>& aSampler,
                          std::int32_t aMaxDepth = 10);

    glm::vec3 ComputeColor(const Ray& ray, Scene& hittable, std::int32_t depth = 0) override;

private:
    const std::int32_t maxDepth;
};
