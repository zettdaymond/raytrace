#pragma once

#include <vector>

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

struct Tile
{
    int x, xEnd, y, yEnd = 0;
};

class Film
{
public:
    Film(const glm::ivec2& aSize)
        : size(aSize)
    {
        buffer.resize(size.x * size.y);
    }

    glm::vec3& operator()(int i, int j)
    {
        return buffer[j * size.x + i];
    }

    const glm::vec3& operator()(int i, int j) const
    {
        return buffer[j * size.x + i];
    }

    glm::ivec2 size;
    std::vector<glm::vec3> buffer;
};
