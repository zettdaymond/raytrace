#pragma once

#include "glm/mat4x4.hpp"
#include "glm/gtc/quaternion.hpp"
#include "glm/gtx/quaternion.hpp"
#include "glm/gtx/matrix_decompose.hpp"
#include "Ray.h"

inline auto Scale(const glm::vec3& scale)
{
    return glm::scale(glm::mat4(1.0f), scale);
}

inline auto Rotate(const glm::quat& rotate)
{
    return glm::toMat4(rotate);
}

inline auto Translate(const glm::vec3& translate)
{
    return glm::translate(glm::mat4(1.0f), translate);
}

class Transform
{
public:
    Transform() = default;

    Transform(const glm::vec3& aPosition, const glm::quat& aRotation, const glm::vec3& aScale)
        : position(aPosition)
        , rotation(aRotation)
        , scale(aScale)
    {
        UpdateMatrix();
    }

    Transform(const glm::mat4& aTransformMtx)
        : transformMtx(aTransformMtx)
    {
        glm::vec3 skew;
        glm::vec4 perspective;

        glm::decompose(transformMtx, scale, rotation, position, skew, perspective);
    }

    void SetPosition(const glm::vec3& aPosition)
    {
        if(position == aPosition)
        {
            return;
        }

        position = aPosition;
        UpdateMatrix();
    }

    void SetRotation(const glm::quat& aRotation)
    {
        if(rotation == aRotation)
        {
            return;
        }

        rotation = aRotation;
        UpdateMatrix();
    }

    void SetScale(const glm::vec3& aScale)
    {
        if(scale == aScale)
        {
            return;
        }

        scale = aScale;
        UpdateMatrix();
    }

    void UpdateMatrix()
    {
        transformMtx = Translate(position) * glm::toMat4(rotation) * Scale(scale);
    }

    glm::mat4 GetTransformMatrix() const
    {
        return transformMtx;
    }

    Transform GetInverse() const
    {
        const auto inverseMtx = glm::inverse(transformMtx);
        return Transform(inverseMtx);
    }

    Transform operator*(const Transform& other) const
    {
        return Transform(transformMtx * other.transformMtx);
    }

    auto operator()(const glm::vec4& v) const
    {
        return transformMtx * v;
    }

    auto operator()(const glm::vec3& v) const
    {
        return transformMtx * glm::vec4(v, 1.0f);
    }

    auto operator()(const Ray& v) const
    {
        const auto newOrigin = transformMtx * glm::vec4(v.origin, 1.0f);
        const auto newDestanation = transformMtx * glm::vec4(v.origin + v.direction, 0.0f);
        return Ray(newOrigin,
                   glm::normalize(newDestanation - newOrigin));
    }

    glm::vec3 getPosition() const
    {
        return position;
    }

    glm::quat getRotation() const
    {
        return rotation;
    }

    glm::vec3 getScale() const
    {
        return scale;
    }

private:
    glm::mat4 transformMtx = glm::mat4(1.0f);

    glm::vec3 position = glm::vec3(0.0f);
    glm::quat rotation = glm::quat(1.0f, 0.0f, 0.0f, 0.0f);
    glm::vec3 scale = glm::vec3(1.0f);
};
