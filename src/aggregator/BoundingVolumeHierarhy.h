#pragma once

#include "Aggregate.h"

class BoundingVolumeHierarhy
        : public Aggregate
{
public:
    enum class eSplitMethod
    {
        kSAH,
        kMiddle,
        kEqualBlocks
    };

    struct PrimitiveInfo
    {
        PrimitiveInfo(std::size_t aPrimitiveArrayIndex,
                      const Bounds3f& aWorldBounds)
            : primitiveArrayIndex(aPrimitiveArrayIndex)
            , worldBounds(aWorldBounds)
            , centroid((aWorldBounds.pMin + aWorldBounds.pMax) * 0.5f)

        {}

        std::size_t primitiveArrayIndex;
        Bounds3f worldBounds;
        glm::vec3 centroid;
    };

    BoundingVolumeHierarhy(const std::vector<std::shared_ptr<Primitive>>& aPrimitives,
                           std::int32_t aMaxPrimitivesInNode = 4,
                           eSplitMethod aSplitMethod = eSplitMethod::kSAH);

    std::optional<HitRecord> hit(const Ray &ray, float t_min, float t_max) override;
    Bounds3f WorldBounds() const override;
    std::optional<ScatterInfo> scatter(const Ray &ray, const HitRecord &hit) override;
    Material *GetMaterial() override;

private:
    struct BVHBuildNode
    {
        void InitLeaf(int first, int n, const Bounds3f &b)
        {
            firstPrimOffset = first;
            nPrimitives = n;
            bounds = b;
        }

        void InitInterior(int axis,
                          std::unique_ptr<BVHBuildNode>&& firstChild,
                          std::unique_ptr<BVHBuildNode>&& secondChild)
        {
            children[0] = std::move(firstChild);
            children[1] = std::move(secondChild);

            bounds = Union(children[0]->bounds, children[1]->bounds);

            splitAxis = axis;
            nPrimitives = 0;
        }

        bool IsLeaf() const
        {
            return !IsInterior();
        }

        bool IsInterior() const
        {
            return nPrimitives == 0;
        }

        Bounds3f bounds;
        std::unique_ptr<BVHBuildNode> children[2] = { nullptr, nullptr };
        int splitAxis, firstPrimOffset, nPrimitives;
    };

    struct LinearBVHNode
    {
        Bounds3f bounds;
        union {
            int primitivesOffset;
            int secondChildOffset;
        };
        uint16_t nPrimitives;
        uint8_t axis;

        bool IsLeaf() const
        {
            return !IsInterior();
        }

        bool IsInterior() const
        {
            return nPrimitives == 0;
        }
    };

    static_assert (sizeof(LinearBVHNode) == 32, "Size of 'LinearBVHNode' must be aligned" );

    std::unique_ptr<BVHBuildNode> RecursiveBuild(std::vector<PrimitiveInfo> &primitiveInfos,
                                                 int start,
                                                 int end,
                                                 int& totalNodes,
                                                 std::vector<std::shared_ptr<Primitive>>& orderedPrimitivess);

    void CreateLeafNode(const std::unique_ptr<BoundingVolumeHierarhy::BVHBuildNode>& node,
                        std::vector<PrimitiveInfo> &primitiveInfos,
                        int start,
                        int end,
                        std::vector<std::shared_ptr<Primitive>>& orderedPrimitivess,
                        int primitivesCount,
                        const Bounds3f &primitivesBounds);

    Bounds3f CalculateBounds(const std::vector<PrimitiveInfo>& primitiveInfos,
                             int start,
                             int end);

    Bounds3f CalculateCentroidBounds(const std::vector<PrimitiveInfo> &primitiveInfos,
                                     int start,
                                     int end) const;

    int FlattenTree(const BVHBuildNode &node, int &offset);

    std::vector<std::shared_ptr<Primitive>> primitives;
    std::int32_t maxPrimitivesInNode;
    eSplitMethod splitMethod;

    std::vector<PrimitiveInfo> primitiveInfos;
    std::vector<LinearBVHNode> linearNodes;
};


