#include "BoundingVolumeHierarhy.h"

namespace SBoundingVolumeHierarhy
{

    struct SAHBucket
    {
        int primitivesCount = 0;
        Bounds3f primitivesBounds;
    };

    constexpr int SAHBucketsPerSplitCount = 12;
    constexpr float SAHIntersectionCost = 1.0f;
    constexpr float SAHTraversalCost = 0.125f; // 1/8

    int PartitionIntoEquallySizedNodes(std::vector<BoundingVolumeHierarhy::PrimitiveInfo> &primitiveInfos,
                                         int start,
                                         int end,
                                         int maxDimension)
    {
        int mid = (start + end) / 2;
        std::nth_element(primitiveInfos.begin() + start,
                         primitiveInfos.begin() + mid,
                         primitiveInfos.begin() + end,
                [maxDimension](const BoundingVolumeHierarhy::PrimitiveInfo &a,
                               const BoundingVolumeHierarhy::PrimitiveInfo &b)
                {
                    return a.centroid[maxDimension] < b.centroid[maxDimension];
                });

        return mid;
    }

}

BoundingVolumeHierarhy::BoundingVolumeHierarhy(const std::vector<std::shared_ptr<Primitive>> &aPrimitives,
                                               int32_t aMaxPrimitivesInNode,
                                               eSplitMethod aSplitMethod)
    : primitives(aPrimitives)
    , maxPrimitivesInNode(aMaxPrimitivesInNode)
    , splitMethod(aSplitMethod)
{
    if(primitives.empty())
    {
        return ;
    }

    for(std::size_t i = 0; i < primitives.size(); i++)
    {
        primitiveInfos.emplace_back(i, primitives[i]->WorldBounds());
    }

    std::unique_ptr<BVHBuildNode> root = nullptr;

    int totalNodes = 0;
    std::vector<std::shared_ptr<Primitive>> orderedPrims;

    if(splitMethod == eSplitMethod::kSAH || splitMethod == eSplitMethod::kMiddle || splitMethod == eSplitMethod::kEqualBlocks)
    {
        root = RecursiveBuild(primitiveInfos, 0, primitives.size(), totalNodes, orderedPrims);
    }
    else
    {
        assert(false && "Not implemented method");
    }

    primitives = std::move(orderedPrims);

    linearNodes.resize(totalNodes);
    int offsetToPtimitive = 0;
    FlattenTree(*root, offsetToPtimitive);
}

std::optional<HitRecord> BoundingVolumeHierarhy::hit(const Ray &ray, float t_min, float t_max)
{
    if (linearNodes.empty())
    {
        return {};
    }
    glm::vec3 invDir = 1.0f / ray.direction;
    int dirIsNeg[3] = {invDir.x < 0, invDir.y < 0, invDir.z < 0};

    int toVisitOffset = 0, currentNodeIndex = 0;
    int nodesToVisit[64];

    std::optional<HitRecord> hit = {};

    while (true)
    {
        const LinearBVHNode& node = linearNodes[currentNodeIndex];
        // Check ray against BVH node
        const bool isNodeHit = node.bounds.FastHit(ray, invDir, dirIsNeg, t_max);
        //const bool isNodeHit = node.bounds.FastHit(ray, t_max);
        if (isNodeHit)
        {
            if (node.IsLeaf())
            {
                for (int i = 0; i < node.nPrimitives; i++)
                {
                    if (const auto& primitiveHit = primitives[node.primitivesOffset + i]->hit(ray, t_min, t_max))
                    {
                        if(primitiveHit->t > t_min && primitiveHit->t < t_max)
                        {
                            hit = primitiveHit;
                            t_max = hit->t;
                        }
                    }
                }
                if (toVisitOffset == 0)
                {
                    break;
                }
                currentNodeIndex = nodesToVisit[--toVisitOffset];
            }
            else
            {
                if (dirIsNeg[node.axis])
                {
                    nodesToVisit[toVisitOffset++] = currentNodeIndex + 1;
                    currentNodeIndex = node.secondChildOffset;
                }
                else {
                    nodesToVisit[toVisitOffset++] = node.secondChildOffset;
                    currentNodeIndex = currentNodeIndex + 1;
                }
            }
        } else {
            if (toVisitOffset == 0)
            {
                break;
            }
            currentNodeIndex = nodesToVisit[--toVisitOffset];
        }
    }
    return hit;
}

std::unique_ptr<BoundingVolumeHierarhy::BVHBuildNode> BoundingVolumeHierarhy::RecursiveBuild(std::vector<BoundingVolumeHierarhy::PrimitiveInfo> &primitiveInfos,
                                                                                             int start,
                                                                                             int end,
                                                                                             int &totalNodes,
                                                                                             std::vector<std::shared_ptr<Primitive> > &orderedPrimitives)
{
    using namespace SBoundingVolumeHierarhy;

    auto node = std::make_unique<BVHBuildNode>();
    totalNodes += 1;

    int primitivesCount = end - start;

    Bounds3f primitivesBounds = CalculateBounds(primitiveInfos, start, end);

    if(primitivesCount == 1)
    {
        int firstPrimitiveOrderedIndex = orderedPrimitives.size();
        for(int i = start; i < end; i++)
        {
            int primitiveIndex = primitiveInfos[i].primitiveArrayIndex;
            orderedPrimitives.push_back(primitives[primitiveIndex]);
        }
        node->InitLeaf(firstPrimitiveOrderedIndex, primitivesCount, primitivesBounds);
        return node;
    }
    else
    {
        Bounds3f centroidBounds = CalculateCentroidBounds(primitiveInfos, start, end);

        int maxDimension = centroidBounds.MaximumExtent();
        int mid = (start + end) / 2;

        bool fallbackToEqualCountsMethod = false;
        if(splitMethod == eSplitMethod::kMiddle)
        {
            float centerAlongAxis = (centroidBounds.pMin[maxDimension] + centroidBounds.pMax[maxDimension]) / 2;
            auto secondPartitionStartIt =
               std::partition(primitiveInfos.begin() + start,
                              primitiveInfos.begin() + end,
               [maxDimension, centerAlongAxis](const PrimitiveInfo &pi)
               {
                   return pi.centroid[maxDimension] < centerAlongAxis;
               });

            mid = std::distance(primitiveInfos.begin(), secondPartitionStartIt);

            if (mid == start || mid == end)
            {
                fallbackToEqualCountsMethod = true;
            }
        }
        if(splitMethod == eSplitMethod::kEqualBlocks || fallbackToEqualCountsMethod)
        {
            mid = PartitionIntoEquallySizedNodes(primitiveInfos, start, end, maxDimension);
        }

        if(splitMethod == eSplitMethod::kSAH)
        {
            if(primitivesCount <= 2)
            {
                mid = PartitionIntoEquallySizedNodes(primitiveInfos, start, end, maxDimension);
            }
            else
            {
                std::array<SAHBucket,SAHBucketsPerSplitCount> sahBackets = {};

                //+
                for(int i = start; i < end; i++)
                {
                    const auto& primitiveCentroid = primitiveInfos[i].centroid;

                    //where bounds.min == (0,0), bounds.max == (1,1)
                    auto centroidInBoundsSpace = centroidBounds.PointInBoundsSpace(primitiveCentroid);

                    int buckedIndex = SAHBucketsPerSplitCount * centroidInBoundsSpace[maxDimension];
                    if(buckedIndex == SAHBucketsPerSplitCount)
                    {
                        buckedIndex = SAHBucketsPerSplitCount - 1;
                    }

                    sahBackets[buckedIndex].primitivesCount += 1;
                    sahBackets[buckedIndex].primitivesBounds =
                            Union(sahBackets[buckedIndex].primitivesBounds,
                                  primitiveInfos[i].worldBounds);
                }

                std::array<float, SAHBucketsPerSplitCount - 1> sahCosts;

                for(std::uint32_t i = 0; i < sahCosts.size(); i++)
                {
                    Bounds3f b0, b1;
                    int count0 = 0, count1 = 0;

                    for(std::uint32_t j = 0; j <= i; j++)
                    {
                        b0 = Union(b0, sahBackets[j].primitivesBounds);
                        count0 += sahBackets[j].primitivesCount;
                    }

                    for(std::uint32_t j = i + 1; j < SAHBucketsPerSplitCount; j++)
                    {
                        b1 = Union(b1, sahBackets[j].primitivesBounds);
                        count1 += sahBackets[j].primitivesCount;
                    }

                    //Compute SAH
                    float firstProbability = b0.SurfaceArea() / primitivesBounds.SurfaceArea();
                    float secondProbability = b1.SurfaceArea() / primitivesBounds.SurfaceArea();
                    sahCosts[i] = SAHTraversalCost + firstProbability * SAHIntersectionCost *  count0
                                                   + secondProbability * SAHIntersectionCost *  count1;
                }

                const auto minIt = std::min_element(sahCosts.begin(), sahCosts.end());
                const auto minSahCostIndex = std::distance(sahCosts.begin(), minIt);
                const float minCost = sahCosts[minSahCostIndex];

                float leafIntersectionCost = SAHIntersectionCost * primitivesCount;
                if(minCost < leafIntersectionCost || primitivesCount > maxPrimitivesInNode)
                {
                    const auto midIt = std::partition(primitiveInfos.begin() + start,
                                                      primitiveInfos.begin() + end ,
                                       [=](const PrimitiveInfo &pi)
                                       {
                                          const auto& centroidInBoundsSpace = centroidBounds.PointInBoundsSpace(pi.centroid);
                                          int buckedIndex = SAHBucketsPerSplitCount * centroidInBoundsSpace[maxDimension];
                                          if(buckedIndex == SAHBucketsPerSplitCount)
                                          {
                                              buckedIndex = SAHBucketsPerSplitCount - 1;
                                          }

                                          return buckedIndex <= minSahCostIndex;
                                       });

                    mid = std::distance(primitiveInfos.begin(), midIt);
                }
                else
                {
                    int firstPrimitiveOrderedIndex = orderedPrimitives.size();
                    for(int i = start; i < end; i++)
                    {
                        int primitiveIndex = primitiveInfos[i].primitiveArrayIndex;
                        orderedPrimitives.push_back(primitives[primitiveIndex]);
                    }
                    node->InitLeaf(firstPrimitiveOrderedIndex, primitivesCount, primitivesBounds);
                    return node;
                }

            }
        }

        node->InitInterior(maxDimension,
                           RecursiveBuild(primitiveInfos, start, mid,
                                          totalNodes, orderedPrimitives),
                           RecursiveBuild(primitiveInfos, mid, end,
                                          totalNodes, orderedPrimitives));
    }

    return node;
}

void BoundingVolumeHierarhy::CreateLeafNode(const std::unique_ptr<BoundingVolumeHierarhy::BVHBuildNode>& node,
                                            std::vector<PrimitiveInfo> &primitiveInfos,
                                            int start,
                                            int end,
                                            std::vector<std::shared_ptr<Primitive> > &orderedPrimitives,
                                            int primitivesCount,
                                            const Bounds3f& primitivesBounds)
{
    int firstPrimitiveOrderedIndex = orderedPrimitives.size();
    for(int i = start; i < end; i++)
    {
        int primitiveIndex = primitiveInfos[i].primitiveArrayIndex;
        orderedPrimitives.push_back(primitives[primitiveIndex]);
    }
    node->InitLeaf(firstPrimitiveOrderedIndex, primitivesCount, primitivesBounds);
}

Bounds3f BoundingVolumeHierarhy::CalculateBounds(const std::vector<PrimitiveInfo> &primitiveInfos, int start, int end)
{
    Bounds3f bounds;
    for(int i = start; i < end; i++ )
    {
        bounds = Union(bounds, primitiveInfos[i].worldBounds);
    }
    return bounds;
}

Bounds3f BoundingVolumeHierarhy::CalculateCentroidBounds(const std::vector<PrimitiveInfo> &primitiveInfos, int start, int end) const
{
    Bounds3f bounds;
    for(int i = start; i < end; i++ )
    {
        bounds = Union(bounds, primitiveInfos[i].centroid);
    }
    return bounds;
}

int BoundingVolumeHierarhy::FlattenTree(const BVHBuildNode& node, int &offset)
{
    auto& linearNode = linearNodes[offset];
    linearNode.bounds = node.bounds;

    int handledNodeOffset = offset;
    offset += 1;
    if(node.IsLeaf())
    {
        linearNode.primitivesOffset = node.firstPrimOffset;
        linearNode.nPrimitives = node.nPrimitives;
    }
    else
    {
        linearNode.axis = node.splitAxis;
        linearNode.nPrimitives = 0;
        FlattenTree(*node.children[0], offset);
        linearNode.secondChildOffset = FlattenTree(*node.children[1], offset);
    }

    return handledNodeOffset;
}

Bounds3f BoundingVolumeHierarhy::WorldBounds() const
{
    return linearNodes.empty() ? Bounds3f{} : linearNodes.front().bounds;
}

std::optional<ScatterInfo> BoundingVolumeHierarhy::scatter(const Ray &ray, const HitRecord &hit)
{
    return hit.primitive ? hit.primitive->scatter(ray, hit) : std::optional<ScatterInfo>{};
}

Material *BoundingVolumeHierarhy::GetMaterial()
{
    return nullptr;
}
