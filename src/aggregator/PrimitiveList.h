#pragma once

#include "Aggregate.h"

class ListAggregate
        : public Aggregate
{
public:
    std::optional<HitRecord> hit(const Ray &r, float t_min, float t_max) override
    {
        std::optional<HitRecord> closestHit = {};
        float closestT = t_max;
        for(const auto& primitive : primitives)
        {
            if(const auto hit = primitive->hit(r, t_min, closestT))
            {
                closestHit = hit;
                closestT = hit.value().t;
            }
        }

        return closestHit;
    }

    std::optional<ScatterInfo> scatter(const Ray &ray, const HitRecord &hit) override
    {
        return {};
    }

    Material *GetMaterial() override
    {
        return nullptr;
    }

    Bounds3f WorldBounds() const override
    {
        return Bounds3f();
    }

    std::vector<std::shared_ptr<Primitive>> primitives;
};
