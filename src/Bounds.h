#pragma once

#include <iostream>

#include "glm/vec3.hpp"
#include "glm/vec2.hpp"
#include "glm/common.hpp"
#include "glm/gtx/compatibility.hpp"

#include "Ray.h"

template <typename T, int TDim>
class Bounds
{
public:
    template<class E>
    using Point = glm::vec<TDim, E, glm::defaultp>;

    using PointT = glm::vec<TDim, T, glm::defaultp>;

    struct BoundingSphere
    {
        Point<T> origin{0.0f};
        float radius = 0.0f;
    };

    Bounds()
    {
        T minNum = std::numeric_limits<T>::lowest();
        T maxNum = std::numeric_limits<T>::max();

        pMin = Point<T>(maxNum);
        pMax = Point<T>(minNum);
    }

    explicit Bounds(const Point<T> &p)
        : pMin(p)
        , pMax(p)
    {}

    Bounds(const Point<T> &p1, const Point<T> &p2)
    {
        pMin = glm::min(p1, p2);
        pMax = glm::max(p1, p2);
    }

    template <typename U>
    explicit operator Bounds<U, TDim>() const
    {
        return Bounds<U, TDim>((Point<U>)pMin, (Point<U>)pMax);
    }

    auto Diagonal() const
    {
        return pMax - pMin;
    }

    T Area() const
    {
        auto d = pMax - pMin;
        return (d.x * d.y);
    }

    int MaximumExtent() const
    {
        auto diag = Diagonal();
        if constexpr (TDim == 2)
        {
            if (diag.x > diag.y)
            {
                return 0;
            }
            else
            {
                return 1;
            }
        }

        if constexpr (TDim == 3)
        {
            if (diag.x > diag.y && diag.x > diag.z)
                return 0;
            else if (diag.y > diag.z)
                return 1;
            else
                return 2;
        }
    }
    inline const Point<T> &operator[](int i) const
    {
        return (i == 0) ? pMin : pMax;
    }

    inline Point<T> &operator[](int i)
    {
        return (i == 0) ? pMin : pMax;
    }

    bool operator==(const Bounds<T, TDim> &b) const
    {
        return b.pMin == pMin && b.pMax == pMax;
    }

    bool operator!=(const Bounds<T, TDim> &b) const
    {
        return !(*this == b);
    }

    auto Lerp(const Point<float> &t) const
    {
        return Point<T>(glm::lerp(pMin.x, pMax.x, t.x),
                         glm::lerp(pMin.y, pMax.y, t.y));
    }

    auto Offset(const Point<T> &p) const
    {
        auto o = p - pMin;
        if (pMax.x > pMin.x)
        {
            o.x /= pMax.x - pMin.x;
        }

        if (pMax.y > pMin.y)
        {
            o.y /= pMax.y - pMin.y;
        }

        return o;
    }

    bool Inside(const Point<T>& p) const
    {
        const auto b1 = glm::greaterThanEqual(p, pMin);
        const auto b2 = glm::lessThanEqual(p, pMin);

        return glm::all(b1) && glm::all(b2);
    }

    auto GetBoundingSphere() const
    {
        const auto center = (pMax + pMin) * 0.5f;
        const auto radius = Inside(center) ? glm::distance(center, pMax) : 0.f;

        return BoundingSphere{ .origin = center,
                               .radius = radius };
    }

    inline bool FastHit(const Ray &ray, float t_max ) const
    {
        float t0 = 0;
        float t1 = t_max;

        for (int i = 0; i < 3; ++i)
        {
            // Update interval for _i_th bounding box slab
            float invRayDir = 1 / ray.direction[i];
            float tNear = (pMin[i] - ray.origin[i]) * invRayDir;
            float tFar = (pMax[i] - ray.origin[i]) * invRayDir;

            // Update parametric interval from slab intersection $t$ values
            if (tNear > tFar) std::swap(tNear, tFar);

            t0 = tNear > t0 ? tNear : t0;
            t1 = tFar < t1 ? tFar : t1;

            if (t0 > t1) return false;
        }
        return true;
    }

    Point<T> PointInBoundsSpace(const Point<T>& p) const
    {
        auto out = p - pMin;

        if (pMax.x > pMin.x) out.x /= (pMax.x - pMin.x);
        if (pMax.y > pMin.y) out.y /= (pMax.y - pMin.y);
        if (pMax.z > pMin.z) out.z /= (pMax.z - pMin.z);

        return out;
    }

    T SurfaceArea() const
    {
        auto d = Diagonal();
        auto area = 2 * (d.x * d.y + d.x * d.z + d.y * d.z);
        return area;
    }

    bool FastHit(const Ray& ray,
                 const glm::vec3 &invDir,
                 const int dirIsNeg[3],
                 const float t_max) const
    {
        static_assert (TDim == 3, "This method implemented only for TDim == 3" );

        auto &bounds = *this;

        float tMin =  (bounds[  dirIsNeg[0]].x - ray.origin.x) * invDir.x;
        float tMax =  (bounds[1-dirIsNeg[0]].x - ray.origin.x) * invDir.x;
        float tyMin = (bounds[  dirIsNeg[1]].y - ray.origin.y) * invDir.y;
        float tyMax = (bounds[1-dirIsNeg[1]].y - ray.origin.y) * invDir.y;

        if (tMin > tyMax || tyMin > tMax)
        {
            return false;
        }
        if (tyMin > tMin)
        {
            tMin = tyMin;
        }

        if (tyMax < tMax)
        {
            tMax = tyMax;
        }

        float tzMin = (bounds[  dirIsNeg[2]].z - ray.origin.z) * invDir.z;
        float tzMax = (bounds[1-dirIsNeg[2]].z - ray.origin.z) * invDir.z;

        if (tMin > tzMax || tzMin > tMax)
        {
            return false;
        }

        if (tzMin > tMin)
        {
            tMin = tzMin;
        }

        if (tzMax < tMax)
        {
            tMax = tzMax;
        }

        return (tMin < t_max) && (tMax > 0);
    }

    friend std::ostream &operator<<(std::ostream &os, const Bounds<T, TDim> &b) {
        os << "[ " << b.pMin << " - " << b.pMax << " ]";
        return os;
    }

    // Bounds2 Public Data
    Point<T> pMin, pMax;
};

using Bounds2i = Bounds<int, 2>;
using Bounds2f = Bounds<float, 2>;

using Bounds3i = Bounds<int, 3>;
using Bounds3f = Bounds<float, 3>;


template<class T, int TDim>
auto Union(const Bounds<T,TDim> &b1, const Bounds<T,TDim> &b2)
{
    Bounds<T,TDim> out;
    out.pMax = glm::max(b1.pMax, b2.pMax);
    out.pMin = glm::min(b1.pMin, b2.pMin);
    return out;
}


template <typename T, int TDim>
auto Union(const Bounds<T,TDim> &b, const typename Bounds<T,TDim>::PointT &p)
{
    Bounds<T,TDim> ret;
    ret.pMin = glm::min(b.pMin, p);
    ret.pMax = glm::max(b.pMax, p);
    return ret;
}
