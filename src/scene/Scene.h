#pragma once

#include <memory>
#include <optional>
#include <vector>

#include "Bounds.h"
#include "HitRecord.h"

class Primitive;
class Ray;
class Light;

class Scene
{
public:
    Scene();

    std::optional<HitRecord> hit(const Ray& r, float t_min, float t_max) const;

    Bounds3f WorldBounds() const;

    std::shared_ptr<Primitive> primitive;
    std::vector<std::shared_ptr<Light>> lights;
};
