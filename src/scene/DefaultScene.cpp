#include "DefaultScene.h"

#include <vector>

#include "glm/vec3.hpp"

#include "Transform.h"
#include "aggregator/PrimitiveList.h"
#include "material/Metal.h"
#include "material/Lambertian.h"
#include "shape/TriangleMesh.h"
#include "shape/Sphere.h"
#include "Primitive.h"
#include "texture/ConstantTexture.h"


const std::vector<glm::vec3> positions = {
        glm::vec3(-0.5f, -0.5f, -0.5f),
        glm::vec3( 0.5f, -0.5f, -0.5f),
        glm::vec3( 0.5f,  0.5f, -0.5f),
        glm::vec3( 0.5f,  0.5f, -0.5f),
        glm::vec3(-0.5f,  0.5f, -0.5f),
        glm::vec3(-0.5f, -0.5f, -0.5f),

        glm::vec3(-0.5f, -0.5f,  0.5f),
        glm::vec3( 0.5f, -0.5f,  0.5f),
        glm::vec3( 0.5f,  0.5f,  0.5f),
        glm::vec3( 0.5f,  0.5f,  0.5f),
        glm::vec3(-0.5f,  0.5f,  0.5f),
        glm::vec3(-0.5f, -0.5f,  0.5f),

        glm::vec3(-0.5f,  0.5f,  0.5f),
        glm::vec3(-0.5f,  0.5f, -0.5f),
        glm::vec3(-0.5f, -0.5f, -0.5f),
        glm::vec3(-0.5f, -0.5f, -0.5f),
        glm::vec3(-0.5f, -0.5f,  0.5f),
        glm::vec3(-0.5f,  0.5f,  0.5f),

        glm::vec3( 0.5f,  0.5f,  0.5f),
        glm::vec3( 0.5f,  0.5f, -0.5f),
        glm::vec3( 0.5f, -0.5f, -0.5f),
        glm::vec3( 0.5f, -0.5f, -0.5f),
        glm::vec3( 0.5f, -0.5f,  0.5f),
        glm::vec3( 0.5f,  0.5f,  0.5f),

        glm::vec3(-0.5f, -0.5f, -0.5f),
        glm::vec3( 0.5f, -0.5f, -0.5f),
        glm::vec3( 0.5f, -0.5f,  0.5f),
        glm::vec3( 0.5f, -0.5f,  0.5f),
        glm::vec3(-0.5f, -0.5f,  0.5f),
        glm::vec3(-0.5f, -0.5f, -0.5f),

        glm::vec3(-0.5f,  0.5f, -0.5f),
        glm::vec3( 0.5f,  0.5f, -0.5f),
        glm::vec3( 0.5f,  0.5f,  0.5f),
        glm::vec3( 0.5f,  0.5f,  0.5f),
        glm::vec3(-0.5f,  0.5f,  0.5f),
        glm::vec3(-0.5f,  0.5f, -0.5f),
    };

const std::vector<std::uint32_t> indices = {
    0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35
};



Scene MakeDefaultScene()
{
    auto world = std::make_shared<ListAggregate>();
    static const auto defaultMaterial = std::make_shared<Metal>(glm::vec3(0.8f, 0.8f, 0.8f), 0.0);

    Transform meshT;
    meshT.SetPosition({1.0, 0.0, 1.0});
    auto mesh = std::make_shared<TriangleMesh>(meshT, positions, indices);
    const uint32_t nTriangles = indices.size() / 3;
    for(uint32_t i = 0; i < nTriangles; i++)
    {
        auto p = std::make_shared<GeometricPrimitive>(std::make_shared<Triangle>(mesh, i * 3),
                                                      defaultMaterial);
        world->primitives.push_back(p);
    }

    world->primitives.push_back(std::make_shared<GeometricPrimitive>(
                                    std::make_shared<Sphere>(glm::vec3(0,0,0), 0.5),
                                    std::make_shared<Lambertian>(std::make_shared<ConstantTexture>(glm::vec3(0.8, 0.3, 0.3)))));
    world->primitives.push_back(std::make_shared<GeometricPrimitive>(
                                    std::make_shared<Sphere>(glm::vec3(0,-100.5,0), 100),
                                    std::make_shared<Lambertian>(std::make_shared<ConstantTexture>(glm::vec3(0.8, 0.8, 0.0)))));
    world->primitives.push_back(std::make_shared<GeometricPrimitive>(
                                    std::make_shared<Sphere>(glm::vec3(1,0,0), 0.5),
                                    std::make_shared<Metal>(glm::vec3(0.8, 0.6, 0.2), 0.3f)));
    world->primitives.push_back(std::make_shared<GeometricPrimitive>(
                                    std::make_shared<Sphere>(glm::vec3(-1,0,0), 0.5),
                                    std::make_shared<Metal>(glm::vec3(0.8, 0.8, 0.8), 0.15f)));
    Scene scene;
    scene.primitive = world;
    scene.lights = {};

    return scene;
}
