﻿#include "SceneLoader.h"

#include <memory>
#include <iostream>
#include <stack>
#include <unordered_set>

#define TINYGLTF_IMPLEMENTATION
#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION
#define TINYGLTF_USE_CPP14

#include "aggregator/PrimitiveList.h"
#include "aggregator/BoundingVolumeHierarhy.h"
#include "tiny_gltf.h"

#include "glm/common.hpp"
#include "glm/vec3.hpp"
#include "glm/gtc/type_ptr.hpp"

#include "Camera.h"
#include "light/PointLight.h"
#include "material/Lambertian.h"
#include "material/Metal.h"
#include "shape/TriangleMesh.h"
#include "texture/ConstantTexture.h"
#include "texture/ImageTexture.h"

void DumpGLTFSceneInfo(const tinygltf::Model& model)
{
    std::cout << "loaded glTF file has:\n"
                << model.accessors.size() << " accessors\n"
                << model.animations.size() << " animations\n"
                << model.buffers.size() << " buffers\n"
                << model.bufferViews.size() << " bufferViews\n"
                << model.materials.size() << " materials\n"
                << model.meshes.size() << " meshes\n"
                << model.nodes.size() << " nodes\n"
                << model.textures.size() << " textures\n"
                << model.images.size() << " images\n"
                << model.skins.size() << " skins\n"
                << model.samplers.size() << " samplers\n"
                << model.cameras.size() << " cameras\n"
                << model.scenes.size() << " scenes\n"
                << model.lights.size() << " lights\n";
}

Transform MakeTransform(const tinygltf::Node& node)
{
    Transform t;
    if(!node.matrix.empty())
    {
        glm::mat4 mtx = glm::make_mat4(node.matrix.data());
        t = mtx;
    }
    else
    {
        glm::vec3 position(0.0f);
        glm::quat rotation(1.0, 0.0, 0.0, 0.0);
        glm::vec3 scale(1.0);

        if(!node.translation.empty())
        {
            position = glm::make_vec3(node.translation.data());
        }
        if(!node.rotation.empty())
        {
            rotation = glm::make_quat(node.rotation.data());
        }
        if(!node.scale.empty())
        {
            scale = glm::make_vec3(node.scale.data());
        }
        //t = Scale(scale) * Rotate(rotation) * Translate(position);
        t = Transform(position, rotation, scale);
    }

    return t;
}

std::shared_ptr<Camera> MakeCamera(const std::shared_ptr<Film>& film,
                                   const tinygltf::Model& model,
                                   int cameraIndex,
                                   const Transform& worldTransform)
{
    const auto& camera = model.cameras[cameraIndex];
    if(camera.type == "perspective")
    {
        float zFar = camera.perspective.zfar == 0 ? 1000.0f : camera.perspective.zfar;
        auto c = std::make_shared<PerspectiveCamera>(film,
                                                      glm::degrees(camera.perspective.yfov),
                                                      camera.perspective.znear,
                                                      zFar);
        c->SetTransform(worldTransform);
        return c;
    }
    else if (camera.type == "orthographic ")
    {
        return nullptr;
    }

    return nullptr;
}

std::shared_ptr<Material> MakeDiffuseMaterial(const glm::vec3& albedo)
{
    return std::make_shared<Lambertian>(std::make_shared<ConstantTexture>(albedo));
}

std::shared_ptr<Material> MakeMetalMaterial(const glm::vec3& albedo)
{
    return std::make_shared<Metal>(albedo, 0.0375);
}

std::shared_ptr<Material> HandleMaterial(const tinygltf::Model& model,
                                         int materialIndex)
{
    static const auto defaultMaterial1 = MakeDiffuseMaterial(glm::vec3(0.8, 0.8, 0.8));
    static const auto defaultMaterial2 = MakeDiffuseMaterial(glm::vec3(0.045, 0.074, 0.245));
    static const auto defaultMaterial3 = MakeMetalMaterial(glm::vec3(0.01, 0.01, 0.01));

    if(materialIndex == -1)
    {
        return defaultMaterial3;
    }

    //TODO: implement pbr material
    auto gltfMaterial = model.materials[materialIndex];
    const bool hasBaseTexture = gltfMaterial.pbrMetallicRoughness.baseColorTexture.index != -1;
    if(hasBaseTexture)
    {
        assert(gltfMaterial.pbrMetallicRoughness.baseColorTexture.texCoord == 0
                && "Npt supported differ texCoord than TEXCOORD0");
        const auto& gltfTexture = model.textures[gltfMaterial.pbrMetallicRoughness.baseColorTexture.index];
        const auto& gltfImage = model.images[gltfTexture.source];

        //TODO: implement different sampling tech
        const auto& sampler = model.samplers[gltfTexture.sampler];

        assert(gltfImage.component != 3 || gltfImage.component == 4);

        const auto textureFormat = gltfImage.component == 3
                ? ImageTexture::eFormat::kRGB
                : ImageTexture::eFormat::kRGBA;

        const auto texture = ImageTexture::Create(glm::ivec2(gltfImage.width, gltfImage.height),
                                                  gltfImage.image,
                                                  textureFormat,
                                                  ImageTexture::eColorSpace::ksRGB,
                                                  ImageTexture::eWrap::kRepeat,
                                                  ImageTexture::eInterpolation::kTrilinear);

        return std::make_shared<Lambertian>(texture);
    }

    return defaultMaterial3;
}

std::vector<std::shared_ptr<Primitive>> HandleMesh(const tinygltf::Model& model,
                                                   const tinygltf::Mesh& mesh,
                                                   const Transform& worldTransform)
{
    std::vector<std::shared_ptr<Primitive>> output;

    for(const auto& meshPrimitive  : mesh.primitives)
    {
        //Indices
        std::vector<std::uint32_t> indices;
        {
            const auto &indicesAccessor = model.accessors[meshPrimitive.indices];
            const auto &bufferView = model.bufferViews[indicesAccessor.bufferView];
            const auto &buffer = model.buffers[bufferView.buffer];
            const auto dataAddress = buffer.data.data() + bufferView.byteOffset +
                    indicesAccessor.byteOffset;
            const auto byteStride = indicesAccessor.ByteStride(bufferView);
            const auto count = indicesAccessor.count;


            switch (indicesAccessor.componentType) {
            case TINYGLTF_COMPONENT_TYPE_BYTE:
            {
                std::vector<char> bytes(count);
                std::memcpy(bytes.data(), dataAddress, count * byteStride);
                for(const auto v : bytes)
                {
                    indices.push_back(v);
                }
                break;
            }

            case TINYGLTF_COMPONENT_TYPE_UNSIGNED_BYTE:
            {
                std::vector<unsigned char> bytes(count);
                std::memcpy(bytes.data(), dataAddress, count * byteStride);
                for(const auto v : bytes)
                {
                    indices.push_back(v);
                }
                break;
            }

            case TINYGLTF_COMPONENT_TYPE_SHORT:
            {
                std::vector<short> bytes(count);
                std::memcpy(bytes.data(), dataAddress, count * byteStride);
                for(const auto v : bytes)
                {
                    indices.push_back(v);
                }
                break;
            }

            case TINYGLTF_COMPONENT_TYPE_UNSIGNED_SHORT:
            {
                std::vector<unsigned short> bytes(count);
                std::memcpy(bytes.data(), dataAddress, count * byteStride);
                for(const auto v : bytes)
                {
                    indices.push_back(v);
                }
                break;
            }

            case TINYGLTF_COMPONENT_TYPE_INT:
            {
                std::vector<int> bytes(count);
                std::memcpy(bytes.data(), dataAddress, count * byteStride);

                for(const auto v : bytes)
                {
                    indices.push_back(v);
                }
                break;
            }

            case TINYGLTF_COMPONENT_TYPE_UNSIGNED_INT:
            {
                std::vector<unsigned int> bytes(count);
                std::memcpy(bytes.data(), dataAddress, count * byteStride);
                for(const auto v : bytes)
                {
                    indices.push_back(v);
                }
                break;
            }
            default:
                break;
            }
        }

        std::vector<glm::vec3> positions;
        std::vector<glm::vec3> normals;
        std::vector<glm::vec2> uv;
        std::vector<glm::vec4> tangent;

        // normals and positions
        assert(meshPrimitive.mode == TINYGLTF_MODE_TRIANGLES);

        for (const auto &attribute : meshPrimitive.attributes)
        {
            const auto attribAccessor = model.accessors[attribute.second];
            const auto &bufferView =
                    model.bufferViews[attribAccessor.bufferView];
            const auto &buffer = model.buffers[bufferView.buffer];
            const auto dataPtr = buffer.data.data() + bufferView.byteOffset +
                    attribAccessor.byteOffset;
            const auto byte_stride = attribAccessor.ByteStride(bufferView);
            const auto count = attribAccessor.count;

//                std::cout << "current attribute has count " << count
//                          << " and stride " << byte_stride << " bytes\n";

            if (attribute.first == "POSITION")
            {
                assert(attribAccessor.type == TINYGLTF_TYPE_VEC3);
                assert(attribAccessor.componentType == TINYGLTF_COMPONENT_TYPE_FLOAT);

                positions.resize(count);

                memcpy(reinterpret_cast<float*>(positions.data()), dataPtr, count * byte_stride );
            }

            if (attribute.first == "NORMAL")
            {
                assert(attribAccessor.type == TINYGLTF_TYPE_VEC3);
                assert(attribAccessor.componentType == TINYGLTF_COMPONENT_TYPE_FLOAT);

                normals.resize(count);

                memcpy(reinterpret_cast<float*>(normals.data()), dataPtr, count * byte_stride );
            }

            if (attribute.first == "TEXCOORD_0")
            {
                assert(attribAccessor.type == TINYGLTF_TYPE_VEC2);
                assert(attribAccessor.componentType == TINYGLTF_COMPONENT_TYPE_FLOAT);

                uv.resize(count);

                memcpy(reinterpret_cast<float*>(uv.data()), dataPtr, count * byte_stride );
            }

            if (attribute.first == "TANGENT")
            {
                assert(attribAccessor.type == TINYGLTF_TYPE_VEC4);
                assert(attribAccessor.componentType == TINYGLTF_COMPONENT_TYPE_FLOAT);

                tangent.resize(count);

                memcpy(reinterpret_cast<float*>(tangent.data()), dataPtr, count * byte_stride );
            }
        }

        auto triangles = MakeTriangleMesh(worldTransform, positions, indices, normals, uv);
        auto material = HandleMaterial(model, meshPrimitive.material);

        for(const auto& triangle : triangles){
            output.push_back(std::make_shared<GeometricPrimitive>(triangle, material));
        }
    }

    return output;
}

std::shared_ptr<Light> HandleLight(const tinygltf::Model& model,
                                   const Transform& parentTransform,
                                   int lightIndex)
{
    auto light = model.lights[lightIndex];
    if( light.type == "point")
    {
        auto color = glm::make_vec3(light.color.data());
        return std::make_shared<PointLight>(parentTransform.getPosition(), color);
    }
    return nullptr;
}

void HandleNode(const tinygltf::Model& model,
                int nodeIndex,
                const Transform& parentTransform,
                std::shared_ptr<ListAggregate>& aggregate,
                std::vector<std::shared_ptr<Light>> lights,
                std::shared_ptr<Camera>& camera,
                const std::shared_ptr<Film>& film)
{
    const auto& node = model.nodes[nodeIndex];

    Transform localTransform = MakeTransform(node);

    const Transform worldTransform = parentTransform * localTransform;

    if(node.mesh != -1)
    {
        const auto primitives = HandleMesh(model, model.meshes[node.mesh], worldTransform);
        aggregate->primitives.insert(aggregate->primitives.end(), primitives.begin(), primitives.end());
    }

    if(node.camera != -1)
    {
        camera = MakeCamera(film, model, node.camera, worldTransform);
    }

    if(node.extensions.count("KHR_lights_punctual"))
    {
        const auto& extention = node.extensions.at("KHR_lights_punctual");
        int lightIndex = extention.Get("light").GetNumberAsInt();
        auto light = HandleLight(model, worldTransform, lightIndex);
        if(light)
        {
            lights.push_back(light);
        }
    }

    for(const auto c : node.children)
    {
        HandleNode(model, c, worldTransform, aggregate, lights, camera, film);
    }
}

std::vector<int> GetNodesWithoutParent(const tinygltf::Model& model, const tinygltf::Scene& scene)
{
    std::unordered_set<int> nodesWithoutParent;

    nodesWithoutParent.insert(scene.nodes.begin(), scene.nodes.end());

    for(const auto nodeID : scene.nodes)
    {
        const auto node = model.nodes[nodeID];

        for(const auto childID: node.children)
        {
            if(nodesWithoutParent.count(childID) > 0)
            {
                nodesWithoutParent.erase(childID);
            }
        }
    }

    std::vector<int> out(nodesWithoutParent.begin(), nodesWithoutParent.end());
    return out;
}

SceneLoadResult LoadScene(const std::string &filePath, const std::shared_ptr<Film>& film)
{
    Scene scene;
    std::shared_ptr<Camera> camera = nullptr;

    tinygltf::Model model;
    tinygltf::TinyGLTF loader;
    std::string err;
    std::string warn;

    bool ret = loader.LoadASCIIFromFile(&model, &err, &warn, filePath);
    //bool ret = loader.LoadBinaryFromFile(&model, &err, &warn, argv[1]); // for binary glTF(.glb)

//    if (!warn.empty()) {
//      printf("Warn: %s\n", warn.c_str());
//    }

//    if (!err.empty()) {
//      printf("Err: %s\n", err.c_str());
//    }

    if (!ret)
    {
        return {};
    }

    if(model.nodes.empty())
    {
        return {};
    }

    std::shared_ptr<ListAggregate> aggregate = std::make_shared<ListAggregate>();

    if(model.defaultScene == -1)
    {
        return {};
    }

    const tinygltf::Scene& gltfScene = model.scenes[model.defaultScene];
    auto nodesWithoutParent = GetNodesWithoutParent(model, gltfScene);

    Transform identityTransform{};

    std::vector<std::shared_ptr<Light>> lights;

    for(const auto node : nodesWithoutParent)
    {
        HandleNode(model, node, identityTransform, aggregate, lights, camera, film );
    }

    auto trueAggregate = std::make_shared<BoundingVolumeHierarhy>(aggregate->primitives,
                                                                  4,
                                                                  BoundingVolumeHierarhy::eSplitMethod::kSAH);
    scene.primitive = trueAggregate;
    scene.lights = std::move(lights);

//    scene.primitive = aggregate;

    return {.camera = camera, .scene = std::move(scene)};
}


