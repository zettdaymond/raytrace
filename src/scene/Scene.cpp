#include "Scene.h"

#include "Primitive.h"

Scene::Scene()
{

}

std::optional<HitRecord> Scene::hit(const Ray &r, float t_min, float t_max) const
{
    return primitive->hit(r, t_min, t_max);
}

Bounds3f Scene::WorldBounds() const
{
    return primitive->WorldBounds();
}
