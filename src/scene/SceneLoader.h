#pragma once

#include <string>

#include "Scene.h"

class Camera;
class Film;

struct SceneLoadResult
{
    std::shared_ptr<Camera> camera;
    Scene scene;
};

SceneLoadResult LoadScene(const std::string& filePath, const std::shared_ptr<Film> &film);
