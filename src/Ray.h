#pragma once

#include "glm/vec3.hpp"
#include "glm/common.hpp"

class Ray
{
public:
    inline Ray() noexcept = default;

    constexpr inline Ray(const glm::vec3& aOrigin, const glm::vec3& aDirection) noexcept
        : origin(aOrigin)
        , direction(aDirection)
    {}

    inline glm::vec3 pointAtParameter(float t) const noexcept
    {
        return origin + t * direction;
    }

    inline glm::vec3 operator()(float t) const noexcept
    {
        return pointAtParameter(t);
    }

    bool hasNaNs() const noexcept
    {
        return glm::any(glm::isnan(origin)) || glm::any(glm::isnan(direction));
    }

    glm::vec3 origin;
    glm::vec3 direction;
};

