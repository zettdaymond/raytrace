#pragma once

#include <memory>

#include "Shape.h"

class Sphere : public Shape
{
public:
    Sphere(const glm::vec3& aCenter, float aRadius)
        : center(aCenter)
        , radius(aRadius)
    {}

    Bounds3f ObjectBounds() const override;
    Bounds3f WorldBounds() const override;

    std::optional<HitRecord> hit(const Ray &r, float t_min, float t_max) const override;

    glm::vec3 center;
    float radius;
};


