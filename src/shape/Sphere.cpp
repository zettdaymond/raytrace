#include "Sphere.h"

#include <array>
#include "glm/geometric.hpp"

std::optional<HitRecord> Sphere::hit(const Ray &r, float t_min, float t_max) const
{
    glm::vec3 oc = r.origin - center;
    float a = glm::dot(r.direction, r.direction);
    float b = 2.0 * glm::dot(oc, r.direction);
    float c = glm::dot(oc, oc) - radius*radius;
    float discriminant = b*b - 4*a*c;

    if (discriminant > 0.0f)
    {
        HitRecord rec;

        //order of signs is really matter. with -b - sqrt(discriminant))/a we get closest point to camera
        //constexpr auto signs = std::experimental::make_array(-1, 1);
        constexpr std::array<int,2> signs ={ -1, 1 };
        for(const auto sign : signs)
        {
            float temp = (-b + sign * sqrt(discriminant))/ (2.0 * a);
            if (temp < t_max && temp > t_min) {
                rec.t = temp;
                rec.point = r.pointAtParameter(rec.t);
                rec.normal = (rec.point - center) / radius;
                return {rec};
            }
        }
    }

    return {};
}

Bounds3f Sphere::ObjectBounds() const
{
    return Bounds3f({-radius, -radius, -radius},
                    {radius, radius, radius});
}

Bounds3f Sphere::WorldBounds() const
{
    return Bounds3f(center - glm::vec3{radius, radius, radius},
                    center + glm::vec3{radius, radius, radius});
}
