#pragma once

#include <optional>

#include "Ray.h"
#include "Bounds.h"
#include "HitRecord.h"

class Shape
{
public:
    virtual ~Shape() = default;
    virtual Bounds3f ObjectBounds() const = 0;
    virtual Bounds3f WorldBounds() const = 0;
    virtual std::optional<HitRecord> hit(const Ray& r, float t_min, float t_max) const = 0;
};


