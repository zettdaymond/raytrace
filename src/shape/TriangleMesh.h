#pragma once

#include <vector>
#include <memory>
#include <cstring>

#include "Shape.h"
#include "Transform.h"

struct TriangleMesh
{
public:
    TriangleMesh(Transform aToWorld,
                 const std::vector<glm::vec3>& aLocalPoints,
                 const std::vector<std::uint32_t>& aIndices,
                 const std::vector<glm::vec3>& aLocalNormals = {},
                 const std::vector<glm::vec2>& aUV = {},
                 const std::vector<glm::vec3>& aLocalTangents = {});

    Transform objectToWorld;
    Transform worldToObject;
    std::vector<glm::vec3> points;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec3> tangents;
    std::vector<glm::vec2> uv;
    std::vector<std::uint32_t> indices;
    std::size_t trianglesCount;
};

class Triangle final
        : public Shape
{
public:
    Triangle (const std::shared_ptr<TriangleMesh>& aMesh, const uint32_t aIndexOffset);

    std::optional<HitRecord> hit(const Ray &r, float t_min, float t_max) const override;

    Bounds3f ObjectBounds() const override;
    Bounds3f WorldBounds() const override;

private:
    std::pair<glm::vec3, glm::vec3> ComputeTrianglePartialDerivatives(const glm::vec3 &p0,
                                                                      const glm::vec3 &p1,
                                                                      const glm::vec3 &p2,
                                                                      const std::array<glm::vec2, 3> &uv);

    std::array<glm::vec2, 3> GetUVs() const;

    std::shared_ptr<TriangleMesh> mesh; //or just reference?
    const std::uint32_t indexOffset;

    glm::vec3 dpdu, dpdv;
};


std::vector<std::shared_ptr<Shape>> MakeTriangleMesh(const Transform &meshT,
                                                     const std::vector<glm::vec3>& positions,
                                                     const std::vector<std::uint32_t>& indices,
                                                     const std::vector<glm::vec3> &normals = {},
                                                     const std::vector<glm::vec2> &uv = {},
                                                     const std::vector<glm::vec3> &tangents = {});
