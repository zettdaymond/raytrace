#include "TriangleMesh.h"

#include <array>

#include "glm/gtx/normal.hpp"

#include "common/Common.h"
#include "material/Lambertian.h"
#include "material/Metal.h"

namespace STriangle
{
    enum class eIntersectionMethod
    {
        kMollerTrumbore,
        kWoop
    };

    constexpr eIntersectionMethod intersectionMethod = eIntersectionMethod::kWoop;

    struct TrinagleIntersection
    {
        bool happens = false;
        float t;
        glm::vec3 baricentric;
        glm::vec3 intersectionPoint;
    };

    glm::vec3 ComputeBaricentric(const glm::vec3& intersection, const glm::vec3& v0, const glm::vec3& v1, const glm::vec3& v2)
    {
        glm::vec3 p0 = v1 - v0;
        glm::vec3 p1 = v2 - v0;
        glm::vec3 p2 = intersection - v0;

        float d00 = glm::dot(p0, p0);
        float d01 = glm::dot(p0, p1);
        float d11 = glm::dot(p1, p1);
        float d20 = glm::dot(p2, p0);
        float d21 = glm::dot(p2, p1);
        float denom = d00 * d11 - d01 * d01;

        float v = (d11 * d20 - d01 * d21) / denom;
        float w = (d00 * d21 - d01 * d20) / denom;
        float u = 1.0f - v - w;

        return {u, v, w};
    }

    //https://en.wikipedia.org/wiki/M%C3%B6ller%E2%80%93Trumbore_intersection_algorithm
    inline TrinagleIntersection fastTriangleIntersection(const Ray& ray, const glm::vec3& v0, const glm::vec3& v1, const glm::vec3& v2)
    {
        using namespace glm;

        vec3 e1 = v1 - v0;
        vec3 e2 = v2 - v0;
        // Вычисление вектора нормали к плоскости
        vec3 pvec = cross(ray.direction, e2);
        float det = dot(e1, pvec);

        // Луч параллелен плоскости
        if (det < 1e-8 && det > -1e-8) {
            return {};
        }

        float inv_det = 1 / det;
        vec3 tvec = ray.origin - v0;
        float u = dot(tvec, pvec) * inv_det;
        if (u < 0 || u > 1) {
            return {};
        }

        vec3 qvec = cross(tvec, e1);
        float v = dot(ray.direction, qvec) * inv_det;
        if (v < 0 || u + v > 1) {
            return {};
        }

        TrinagleIntersection i;
        i.t = dot(e2, qvec) * inv_det;
        i.intersectionPoint = ray.origin + ray.direction * i.t;
        i.baricentric = ComputeBaricentric(i.intersectionPoint, v0, v1, v2);
        i.happens = true;

        return i;
    }

    inline TrinagleIntersection presiceTriangleIntersection(const Ray& r, const glm::vec3& v0, const glm::vec3& v1, const glm::vec3& v2)
    {
        // At first, we need to translate space into ray-triangle intersection space.
        // In this space ray origin is (0,0,0) and direction lay in z+ axis
        glm::vec3 v0IntersectSpace = v0 - r.origin;
        glm::vec3 v1IntersectSpace = v1 - r.origin;
        glm::vec3 v2IntersectSpace = v2 - r.origin;

        // The find largest dimension and map it to z
        uint8_t kz = MaxDimension(glm::abs(r.direction));
        uint8_t kx = kz + 1;

        if (kx == 3)
        {
            kx = 0;
        }

        uint8_t ky = kx + 1;
        if (ky == 3)
        {
            ky = 0;
        }

        glm::vec3 rayDirection = Permute(r.direction, kx, ky, kz);
        v0IntersectSpace = Permute(v0IntersectSpace, kx, ky, kz);
        v1IntersectSpace = Permute(v1IntersectSpace, kx, ky, kz);
        v2IntersectSpace = Permute(v2IntersectSpace, kx, ky, kz);

        //Apply Shier transform to lay ray direction over z = 1 axis
        float Sx = -rayDirection.x / rayDirection.z;
        float Sy = -rayDirection.y / rayDirection.z;
        float Sz = 1.f / rayDirection.z;
        v0IntersectSpace.x += Sx * v0IntersectSpace.z;
        v0IntersectSpace.y += Sy * v0IntersectSpace.z;
        v1IntersectSpace.x += Sx * v1IntersectSpace.z;
        v1IntersectSpace.y += Sy * v1IntersectSpace.z;
        v2IntersectSpace.x += Sx * v2IntersectSpace.z;
        v2IntersectSpace.y += Sy * v2IntersectSpace.z;

        //After transformation, we morph 3D intersection problem into 2D problem.
        //Is ray origin (0,0) lay inside triangle v0,v1,v2?

        //Compute edge functions
        float e0 = v1IntersectSpace.x * v2IntersectSpace.y - v1IntersectSpace.y * v2IntersectSpace.x;
        float e1 = v2IntersectSpace.x * v0IntersectSpace.y - v2IntersectSpace.y * v0IntersectSpace.x;
        float e2 = v0IntersectSpace.x * v1IntersectSpace.y - v0IntersectSpace.y * v1IntersectSpace.x;

        //We must really care, if some of edge functions equals zero,
        //Cause that means that ray hit triangle accuratetly in edge
        const bool rayHitTriangleAtEdge = (e0 == 0.0f || e1 == 0.0f || e2 == 0.0f);
        if(rayHitTriangleAtEdge)
        {
            e0 = (float)((double)v1IntersectSpace.x * (double)v2IntersectSpace.y -
                          (double)v1IntersectSpace.y * (double)v2IntersectSpace.x);
            e1 = (float)((double)v2IntersectSpace.x * (double)v0IntersectSpace.y -
                          (double)v2IntersectSpace.y * (double)v0IntersectSpace.x);
            e2 = (float)((double)v0IntersectSpace.x * (double)v1IntersectSpace.y -
                          (double)v0IntersectSpace.y * (double)v1IntersectSpace.x);
        }

        const bool edgeFuncsSignsDiffers = (e0 < 0.0f || e1 < 0.0f || e2 < 0.0f) && (e0 > 0.0f || e1 > 0.0f || e2 > 0.0f);
        const bool rayOriginOutsideTriangle = edgeFuncsSignsDiffers;
        if (rayOriginOutsideTriangle)
        {
            return {};
        }

        float det = e0 + e1 + e2;
        if (det == 0.0f)
        {
            return {};
        }

        v0IntersectSpace.z *= Sz;
        v1IntersectSpace.z *= Sz;
        v2IntersectSpace.z *= Sz;

        //Compute baricentric coordinates of intersection point
        float b0 = e0 / det;
        float b1 = e1 / det;
        float b2 = e2 / det;

        //Compute t;
        float t = b0 * v0IntersectSpace.z + b1 * v1IntersectSpace.z + b2 * v2IntersectSpace.z;

        return TrinagleIntersection{.happens = true,
                                    .t = t,
                                    .baricentric = glm::vec3(b0,b1,b2),
                                    .intersectionPoint = (b0 * v0 + b1 * v1 + b2 * v2)};
    }
}

Triangle::Triangle(const std::shared_ptr<TriangleMesh> &aMesh, const uint32_t aIndexOffset)
    : mesh(aMesh)
    , indexOffset(aIndexOffset)
{
    const auto i0 = mesh->indices[ indexOffset ];
    const auto i1 = mesh->indices[ indexOffset + 1];
    const auto i2 = mesh->indices[ indexOffset + 2];

    const glm::vec3& v0 = mesh->points[ i0 ];
    const glm::vec3& v1 = mesh->points[ i1 ];
    const glm::vec3& v2 = mesh->points[ i2 ];


    std::tie(dpdu, dpdv) = ComputeTrianglePartialDerivatives(v0, v1, v2, GetUVs());
}

std::optional<HitRecord> Triangle::hit(const Ray &r, float t_min, float t_max) const
{
    using namespace STriangle;

    const auto i0 = mesh->indices[ indexOffset ];
    const auto i1 = mesh->indices[ indexOffset + 1];
    const auto i2 = mesh->indices[ indexOffset + 2];

    const glm::vec3& v0 = mesh->points[ i0 ];
    const glm::vec3& v1 = mesh->points[ i1 ];
    const glm::vec3& v2 = mesh->points[ i2 ];

    TrinagleIntersection i;
    if constexpr(intersectionMethod == eIntersectionMethod::kMollerTrumbore)
    {
        i = fastTriangleIntersection(r, v0, v1, v2 );
    }
    else
    {
        i = presiceTriangleIntersection(r,v0,v1,v2);
    }

    if(i.happens == false)
    {
        return {};
    }

    const auto& [_, t, baricentric, intersection] = i;

    if(t < t_min || t > t_max)
    {
        return {};
    }

    const glm::vec2& uv0 = mesh->uv[ i0 ];
    const glm::vec2& uv1 = mesh->uv[ i1 ];
    const glm::vec2& uv2 = mesh->uv[ i2 ];

    HitRecord out;
    out.point = intersection;
    out.t = t;
    out.uv = baricentric.x * uv0 +
             baricentric.y * uv1 +
             baricentric.z * uv2;

    out.normal = glm::triangleNormal(v0, v1, v2);
    if(glm::dot(out.normal, r.direction) > 0.0f)
    {
        out.normal = -out.normal;
    }

    //calculate values for shading
    if(mesh->normals.empty() == false)
    {
        out.shading.normal = baricentric.x * mesh->normals[i0] +
                             baricentric.y * mesh->normals[i1] +
                             baricentric.z * mesh->normals[i2];
        if(glm::length2(out.shading.normal) > 0)
        {
            out.shading.normal = glm::normalize(out.shading.normal);
            if(glm::dot(out.shading.normal, r.direction) > 0.0f)
            {
                out.shading.normal = -out.shading.normal;
            }
        }
        else
        {
            out.shading.normal = out.normal;
        }
    }
    else
    {
        out.shading.normal = out.normal;
    }

    return out;
}

std::pair<glm::vec3,glm::vec3> Triangle::ComputeTrianglePartialDerivatives(const glm::vec3& p0,
                                                                           const glm::vec3& p1,
                                                                           const glm::vec3& p2,
                                                                           const std::array<glm::vec2, 3>& uv)
{
    glm::vec3 dpdu, dpdv;

    glm::vec2 duv02 = uv[0] - uv[2];
    glm::vec2 duv12 = uv[1] - uv[2];
    glm::vec3 dp02 = p0 - p2;
    glm::vec3 dp12 = p1 - p2;

    float determinant = duv02[0] * duv12[1] - duv02[1] * duv12[0];

    const bool degenerateUV = std::abs(determinant) < 1e-8f;

    if (!degenerateUV)
    {
        float invdet = 1 / determinant;
        dpdu = (duv12[1] * dp02 - duv02[1] * dp12) * invdet;
        dpdv = (-duv12[0] * dp02 + duv02[0] * dp12) * invdet;
    }
    if (degenerateUV || glm::length2(glm::cross(dpdu, dpdv)) == 0.f)
    {
        // Handle zero determinant for triangle partial derivative matrix
        glm::vec3 ng = glm::cross(p2 - p0, p1 - p0);
        if (glm::length2(ng) == 0.f)
        {
            // The triangle is actually degenerate; the intersection is
            // bogus.
            //return false;
        }

        MakeCoordinateSystem(glm::normalize(ng), dpdu, dpdv);
    }

    return {dpdu, dpdv};
}

std::array<glm::vec2, 3> Triangle::GetUVs() const
{
    std::array<glm::vec2, 3> out;
    if (!mesh->uv.empty())
    {
        out[0] = mesh->uv[mesh->indices[indexOffset]];
        out[1] = mesh->uv[mesh->indices[indexOffset + 1]];
        out[2] = mesh->uv[mesh->indices[indexOffset + 2]];
    }
    else
    {
        out[0] = glm::vec2(0, 0);
        out[1] = glm::vec2(1, 0);
        out[2] = glm::vec2(1, 1);
    }

    return out;
}

std::vector<std::shared_ptr<Shape>> MakeTriangleMesh(const Transform &meshT,
                                                     const std::vector<glm::vec3> &positions,
                                                     const std::vector<uint32_t> &indices,
                                                     const std::vector<glm::vec3> &normals,
                                                     const std::vector<glm::vec2> &uv,
                                                     const std::vector<glm::vec3> &tangents)
{
    std::vector<std::shared_ptr<Shape>> shapes;

    auto mesh = std::make_shared<TriangleMesh>(meshT, positions, indices, normals, uv, tangents);
    const std::size_t nTriangles = indices.size() / 3;
    for(std::size_t i = 0; i < nTriangles; i++)
    {
        auto p = std::make_shared<Triangle>(mesh, i * 3);
        shapes.push_back(p);
    }

    return shapes;
}

Bounds3f Triangle::ObjectBounds() const
{
    auto b = WorldBounds();

    b.pMax = mesh->worldToObject(glm::vec4(b.pMax, 1.0));
    b.pMin = mesh->worldToObject(glm::vec4(b.pMin, 1.0));

    return b;
}

Bounds3f Triangle::WorldBounds() const
{
    const auto &v0 = mesh->points[mesh->indices[indexOffset]];
    const auto &v1 = mesh->points[mesh->indices[indexOffset + 1]];
    const auto &v2 = mesh->points[mesh->indices[indexOffset + 2]];

    return Union(Bounds3f(v0, v1), Bounds3f(v1,v2));
}

TriangleMesh::TriangleMesh(Transform aToWorld, const std::vector<glm::vec3> &aLocalPoints, const std::vector<uint32_t> &aIndices, const std::vector<glm::vec3> &aLocalNormals, const std::vector<glm::vec2> &aUV, const std::vector<glm::vec3> &aLocalTangents)
    : objectToWorld(aToWorld)
    , worldToObject(aToWorld.GetInverse())
    , indices(aIndices)
    , trianglesCount(aIndices.size() / 3)
{
    assert(aIndices.size() % 3 == 0);

    if(aLocalPoints.size() > 0 && aIndices.size() > 0)
    {
        std::transform(aLocalPoints.begin(), aLocalPoints.end(), std::back_inserter(points),
                       [this](const auto& localPoint)
        {
            return glm::vec3(objectToWorld(glm::vec4(localPoint, 1.0f)));
        });
    }

    if(aLocalNormals.size() > 0)
    {
        std::transform(aLocalNormals.begin(), aLocalNormals.end(), std::back_inserter(normals),
                       [this](const auto& localNormal)
        {
            return glm::vec3(objectToWorld(glm::vec4(localNormal, 0.0f)));
        });
    }

    if(aLocalTangents.size() > 0)
    {
        std::transform(aLocalTangents.begin(), aLocalTangents.end(), std::back_inserter(tangents),
                       [this](const auto& localTangent)
        {
            return glm::vec3(objectToWorld(glm::vec4(localTangent, 0.0f)));
        });
    }

    if(aUV.size())
    {
        uv.resize(aUV.size());
        static_assert(std::is_pod<decltype(uv)::value_type>::value);
        static_assert (sizeof(decltype(uv)::value_type) == sizeof(float) * 2);
        std::memcpy(uv.data(), aUV.data(), sizeof(decltype(uv)::value_type) * aUV.size());
    }
}
