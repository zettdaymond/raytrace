#include "StratifiedSampler.h"

#include <random>
#include "common/RandomGenerator.h"

#include "thirdparty/pcg-cpp/include/pcg_random.hpp"

struct StratifiedSampler::StratifiedSamplerImpl
{
    RandomGenerator rng;

    inline auto UniformFloat()
    {
        return rng.UniformFloat();
    }
};

namespace SStratifiedSampler
{
    static const float OneMinusEpsilon = 0.99999994;
}

StratifiedSampler::StratifiedSampler(const glm::ivec2 &aPixelSamples, bool aRandomJitterSamples)
    : PixelSampler(aPixelSamples.x * aPixelSamples.y, 1)
    , mImpl()
    , randomJitterSamples(aRandomJitterSamples)
    , pixelSamples(aPixelSamples)
{
}

StratifiedSampler::~StratifiedSampler() = default;

std::unique_ptr<Sampler> StratifiedSampler::Clone(int seed) const
{
    auto sampler = std::make_unique<StratifiedSampler>(pixelSamples, randomJitterSamples);
    sampler->mImpl->rng.SetSeed(seed);

    return sampler;
}

void StratifiedSampler::StartPixel(const glm::ivec2 &pixel)
{
    using namespace SStratifiedSampler;

    PixelSampler::StartPixel(pixel);

    for (auto& samples : samples1D)
    {
        float invNSamples = 1.0f / (samples.size());

        for (std::size_t i = 0; i < samples.size(); ++i)
        {
            float delta = randomJitterSamples ? mImpl->UniformFloat() : 0.5f;
            samples[i] = std::min((i + delta) * invNSamples, OneMinusEpsilon);
        }
    }

    for (auto& samples : samples2D)
    {
        auto invNSamples = 1.0f / glm::vec2(pixelSamples);

        int k = 0;
        for (int x = 0; x < pixelSamples.x; ++x)
        {
            for(int y = 0; y < pixelSamples.y; ++y)
            {
                float jx = randomJitterSamples ? mImpl->UniformFloat() : 0.5f;
                float jy = randomJitterSamples ? mImpl->UniformFloat() : 0.5f;

                samples[k].x = std::min((x + jx) * invNSamples.x, OneMinusEpsilon);
                samples[k].y = std::min((y + jy) * invNSamples.y, OneMinusEpsilon);

                k++;
            }
        }
    }
}
