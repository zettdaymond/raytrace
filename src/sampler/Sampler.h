#pragma once

#include <memory>
#include <vector>

#include <glm/vec2.hpp>

class Sampler
{
public:
    enum class Dimension2D
    {
        Film,
    };

    Sampler(std::size_t aSamplesPerPixel)
        : samplesPerPixel(aSamplesPerPixel)
    {}
    virtual ~Sampler() = default;

    glm::vec2 GetUVCameraSample(const glm::ivec2& pixel)
    {
        return glm::vec2(pixel) + Get2D(Dimension2D::Film);
    }

    virtual glm::vec2 Get2D(Dimension2D d) = 0;

    virtual void StartPixel(const glm::ivec2 &pixel)
    {
        currentPixel = pixel;
        currentPixelSampleIndex = 0;
    }

    virtual bool StartNextSample()
    {
        currentPixelSampleIndex += 1;
        return currentPixelSampleIndex < samplesPerPixel;
    }

    virtual std::unique_ptr<Sampler> Clone(int seed) const = 0;

    std::size_t GetSamplesPerPixel() const
    {
        return samplesPerPixel;
    }

protected:
    const std::size_t samplesPerPixel;

    //Iteration states
    glm::ivec2 currentPixel;
    glm::size_t currentPixelSampleIndex = 0;
};


class PixelSampler
        : public Sampler
{
public:
    PixelSampler(std::size_t aSamplesPerPixel, std::size_t numDimensions)
        : Sampler(aSamplesPerPixel)
    {
        for (int i = 0; i < numDimensions; ++i) {
            samples1D.push_back(std::vector<float>(samplesPerPixel));
            samples2D.push_back(std::vector<glm::vec2>(samplesPerPixel));
        }
    }

    glm::vec2 Get2D(Dimension2D d) override
    {
        assert(static_cast<std::size_t>(d) < samples2D.size());
        return samples2D[static_cast<std::size_t>(d)][currentPixelSampleIndex];
    }

protected:
    std::vector<std::vector<float>> samples1D;
    std::vector<std::vector<glm::vec2>> samples2D;

};

