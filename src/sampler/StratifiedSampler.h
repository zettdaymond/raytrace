#pragma once

#include "Sampler.h"

#include "common/Pimpl.h"

class StratifiedSampler final
        : public PixelSampler
{
public:
    StratifiedSampler(const glm::ivec2& aPixelSamples, bool aRandomJitterSamples);
    ~StratifiedSampler() override;

    void StartPixel(const glm::ivec2 &pixel) override;

    std::unique_ptr<Sampler> Clone(int seed) const override;
private:
    struct StratifiedSamplerImpl;
    Pimpl<StratifiedSamplerImpl, 512> mImpl;

    bool randomJitterSamples = false;
    glm::ivec2 pixelSamples;
};


