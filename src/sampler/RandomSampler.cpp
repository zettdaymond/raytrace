#include "RandomSampler.h"

#include <random>

#include "common/RandomGenerator.h"

struct RandomSampler::RandomSamplerImpl
{
   RandomGenerator rng;
};

RandomSampler::RandomSampler(std::size_t samplesPerPixel, std::optional<int> seed)
    : Sampler(samplesPerPixel)
{
    if(seed)
    {
        mImpl->rng.SetSeed(*seed);
    }
}

glm::vec2 RandomSampler::Get2D(Sampler::Dimension2D d)
{
    return glm::vec2(mImpl->rng.UniformFloat(), mImpl->rng.UniformFloat());
}

RandomSampler::~RandomSampler() = default;


std::unique_ptr<Sampler> RandomSampler::Clone(int seed) const
{
    return std::make_unique<RandomSampler>(samplesPerPixel, seed);
}
