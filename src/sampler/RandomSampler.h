#pragma once

#include <optional>

#include "Sampler.h"

#include "common/Pimpl.h"

class RandomSampler final
        : public Sampler
{
public:
    RandomSampler(std::size_t samplesPerPixel, std::optional<int> seed = {});
    ~RandomSampler() override;

    glm::vec2 Get2D(Dimension2D d) override;

    std::unique_ptr<Sampler> Clone(int seed) const override;
private:
    struct RandomSamplerImpl;
    Pimpl<RandomSamplerImpl, 512> mImpl;
};
