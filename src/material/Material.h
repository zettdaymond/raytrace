#pragma once

#include <optional>

#include <glm/vec3.hpp>

#include "Ray.h"

struct HitRecord;

struct ScatterInfo
{
    glm::vec3 attenuation;
    Ray scatteredRay;
};

class Material
{
public:
    virtual ~Material() = default;

    virtual std::optional<ScatterInfo> scatter(const Ray& ray, const HitRecord& hit) = 0;
};
