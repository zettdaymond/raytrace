#pragma once

#include <memory>

#include "Material.h"

class Texture;

class Lambertian : public Material
{
public:
    Lambertian(const std::shared_ptr<Texture> &aTexture)
        : texture(aTexture)
    {}

    std::optional<ScatterInfo> scatter(const Ray &ray, const HitRecord &hit) override;

    std::shared_ptr<Texture> texture;
};
