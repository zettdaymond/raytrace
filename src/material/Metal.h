#pragma once

#include "Material.h"

class Ray;

class Metal : public Material
{
public:
    Metal(glm::vec3 aAlbedo, float aPowerOfRandomisation)
        : albedo(aAlbedo)
        , powerOfRandomisation(aPowerOfRandomisation)
    {}

    std::optional<ScatterInfo> scatter(const Ray &ray, const HitRecord &hit) override;

    glm::vec3 albedo;
    float powerOfRandomisation = 0.0f;
};
