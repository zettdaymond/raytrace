#include "Metal.h"

#include "glm/geometric.hpp"

#include "Ray.h"
#include "shape/Shape.h"
#include "common/Utilities.h"

std::optional<ScatterInfo> Metal::scatter(const Ray &ray, const HitRecord &hit)
{
    auto reflected = glm::reflect(normalize(ray.direction), hit.shading.normal);

    const bool angleBetweenNormalAndReflectIsStrict = glm::dot(reflected, hit.shading.normal) > 0.f;

    if(angleBetweenNormalAndReflectIsStrict)
    {
        ScatterInfo info;
        info.attenuation = albedo;
        info.scatteredRay = Ray(hit.point, reflected + powerOfRandomisation * randomInUnitSphere());

        return {info};
    }

    return {};
}
