#include "Lambertian.h"

#include "Ray.h"
#include "shape/Shape.h"
#include "common/Utilities.h"

#include "texture/Texture.h"

std::optional<ScatterInfo> Lambertian::scatter(const Ray &ray, const HitRecord &hit)
{
    ScatterInfo out;

    auto randomTarget = hit.point + hit.shading.normal + randomInUnitSphere();
    out.scatteredRay = Ray(hit.point, glm::normalize(randomTarget - hit.point));
    out.attenuation = texture->GetValue(hit);

    return { out };
}
