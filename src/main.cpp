#include <iostream>
#include <fstream>
#include <thread>
#include <vector>
#include <sstream>
#include <filesystem>

#include "cxxopts.hpp"

#include "common/Loger.h"
#include "Film.h"
#include "Camera.h"
#include "Integrator.h"
#include "sampler/StratifiedSampler.h"
#include "sampler/RandomSampler.h"
#include "aggregator/Aggregate.h"

#include "scene/DefaultScene.h"
#include "scene/SceneLoader.h"
#include "scene/Scene.h"

namespace fs = std::filesystem;

std::ostringstream WritoPPMImageToBuffer(const Film& film)
{
    std::ostringstream out;

    out << "P3" << std::endl
        << film.size.x << " " << film.size.y << std::endl
        << "255" << std::endl;

    for(int j = 0; j < film.size.y; j++)
    {
        for(int i = 0; i < film.size.x; i++)
        {
            const glm::ivec3 outputColor = film(i,j) * 255.99f;

            out << outputColor.r << " " << outputColor.g << " " << outputColor.b << std::endl;
        }
    }

    return out;
}

struct Config
{
    glm::ivec2 outputResolution = glm::ivec2(1920, 1080);
    std::optional<fs::path> outputFile = "example.ppm";
    std::optional<fs::path> scenePath = {};
};

int main(int argc, char *argv[])
{
    Loger::InitConsoleLoging();

    LOGI("Starting raytrace 0.0.1");

    Config config;
    cxxopts::Options options("raytrace", "My simple raytracer");

    options.add_options()
            ("x,resolution_x", "", cxxopts::value<int>()->default_value(std::to_string(config.outputResolution.x)))
            ("y,resolution_y", "", cxxopts::value<int>()->default_value(std::to_string(config.outputResolution.y)))
            ("s,scene", "gltf 2.0 embedded scene file path", cxxopts::value<std::string>())
            ("o,output", "Output file path", cxxopts::value<std::string>()->default_value(config.outputFile->string()))
            ("h,help", "Print this message");

    auto result = options.parse(argc, argv);

    if (result.count("help"))
    {
        std::cout << options.help() << std::endl;
        std::exit(0);
    }

    config.outputResolution.x = result["resolution_x"].as<int>();
    config.outputResolution.y = result["resolution_y"].as<int>();
    if(result.count("scene"))
    {
        config.scenePath = result["scene"].as<std::string>();
    }

    config.outputFile = result["output"].as<std::string>();

    auto film = std::make_shared<Film>( glm::ivec2(config.outputResolution.x,
                                                   config.outputResolution.y) );
    std::shared_ptr<Camera> camera = nullptr;

    Scene world;
    if(config.scenePath && fs::exists(*config.scenePath))
    {
        const auto slr = LoadScene(config.scenePath->string(),film);
        camera = slr.camera;
        world = slr.scene;
    }
    else
    {
        world = MakeDefaultScene();
    }

    if(camera == nullptr)
    {
        camera = std::make_shared<PerspectiveCamera>(film, 45, 0.01f, 1000.f);
        Transform t;
        t.SetPosition({-0.25f, 0.0f, 3.0f});
        //t.SetPosition({-2.5f, 0.65f, 1.5f});
        //t.SetRotation(glm::quat(glm::vec3(glm::radians(-15.0f), glm::radians(-55.f), 0.0f)));
        camera->SetTransform(t);
    }

    PathTracerIntegrator integrator(camera, std::make_shared<StratifiedSampler>(glm::ivec2{16,16}, true));
//    SamplerIntegrator integrator(camera, std::make_shared<RandomSampler>(samplesPerPixel));
    integrator.Render(world);


    auto out = WritoPPMImageToBuffer(camera->GetFilm());

    std::ofstream outFile(*config.outputFile,std::ofstream::out);
    outFile << out.str();
    outFile.close();

    return 0;
}
