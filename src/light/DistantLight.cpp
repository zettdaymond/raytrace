#include "DistantLight.h"

#include "scene/Scene.h"

DirectionalLight::DirectionalLight(const glm::vec3 &aDirection, const glm::vec3 &aIntencityPerPoint)
    : Light(eFlags::kDeltaDirection, Transform{}, 1)
    , direction(glm::normalize(aDirection))
    , intencityPerPoint(aIntencityPerPoint)
{}

LightSample DirectionalLight::SampleLight(const HitRecord &hit)
{
    LightSample sample;
    sample.pdf = 1.f;
    sample.radiance = intencityPerPoint;
    sample.incidentDirection = direction;

    const auto pointOutsideBSphere = hit.point + direction * 2.0f * worldBoundsSphere.radius;
    sample.visTester = VisibilityTester(hit.point,
                                        pointOutsideBSphere);

    return sample;
}

glm::vec3 DirectionalLight::TotalPower() const
{
    //Area of bounding sphere's disk at radius R => L * Pi * R^2
    return intencityPerPoint *
            glm::pi<float>() *
            worldBoundsSphere.radius *
            worldBoundsSphere.radius;
}

void DirectionalLight::Preprocess(const Scene &scene)
{
    worldBoundsSphere = scene.WorldBounds().GetBoundingSphere();
}
