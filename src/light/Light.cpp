#include "Light.h"

#include "scene/Scene.h"

Light::Light(Light::Flags aFlags, const Transform &aLocalToWorld, uint32_t aSamplesCount)
    : flags(aFlags)
    , localToWorld(aLocalToWorld)
    , worldToLocal(aLocalToWorld.GetInverse())
    , samplesCount(aSamplesCount)
{}



bool VisibilityTester::isVisible(Scene &scene)
{
    const auto record = scene.hit(Ray{startPoint, glm::normalize(endPoint - startPoint)},
                                  0.001f,
                                  std::numeric_limits<float>::max());
    return static_cast<bool>(record);
}


void Light::Preprocess(const Scene &scene)
{}
