#pragma once

#include "common/Flags.h"
#include "Transform.h"

struct HitRecord;
class Scene;


struct VisibilityTester
{
    VisibilityTester() = default;

    VisibilityTester(const glm::vec3& aStartPoint, const glm::vec3& aEndPoint)
        : startPoint(aStartPoint)
        , endPoint(aEndPoint)
    {}

    glm::vec3 startPoint;
    glm::vec3 endPoint;

    bool isVisible(Scene& scene);
};

struct LightSample
{
    LightSample() = default;

    float pdf = 1.f;
    glm::vec3 radiance{0.0f};
    glm::vec3 incidentDirection{0.0f, 0.0f, 0.0f};
    VisibilityTester visTester;
};

class Light
{
public:
    enum class eFlags
    {
        kDeltaPosition = 1,
        kDeltaDirection = 2,
        kArea = 4,
        kInfinite = 8
    };

    using Flags = Flags<eFlags, std::underlying_type_t<eFlags>>;

    Light(Flags aFlags, const Transform& aLocalToWorld, std::uint32_t aSamplesCount);

    virtual ~Light() = default;

    bool IsDeltaDistributedLight() const
    {
        return flags & eFlags::kDeltaPosition ||
               flags & eFlags::kDeltaDirection;
    }

    virtual void Preprocess(const Scene& scene);

    virtual LightSample SampleLight(const HitRecord& hit) = 0;
    virtual glm::vec3 TotalPower() const = 0;

protected:
    Flags flags;
    Transform localToWorld;
    Transform worldToLocal;
    std::uint32_t samplesCount;
};
