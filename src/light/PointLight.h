#pragma once

#include "Light.h"

class PointLight
        : public Light
{
public:
    PointLight(const glm::vec3& aPosition, const glm::vec3& aIntencityPerUnitSolidAngle);

    LightSample SampleLight(const HitRecord &hit) override;

    glm::vec3 TotalPower() const override;

private:
    glm::vec3 position;
    glm::vec3 intencityPerUnitSolidAngle;
};
