#include "AreaLight.h"

AreaLight::AreaLight(const Transform &aTransform, int nSamples)
    : Light(eFlags::kArea, aTransform, nSamples)
{}

LightSample DiffuseAreaLight::SampleLight(const HitRecord &hit)
{
}

glm::vec3 DiffuseAreaLight::TotalPower() const
{
}

glm::vec3 DiffuseAreaLight::L() const
{
}
