#include "PointLight.h"

#include "shape/Shape.h"


PointLight::PointLight(const glm::vec3 &aPosition, const glm::vec3 &aIntencityPerUnitSolidAngle)
    : Light(eFlags::kDeltaPosition, Translate(aPosition), 1)
    , position(aPosition)
    , intencityPerUnitSolidAngle(aIntencityPerUnitSolidAngle)
{}

LightSample PointLight::SampleLight(const HitRecord &hit)
{
    LightSample sample;
    sample.pdf = 1.0f;
    sample.incidentDirection = glm::normalize(position - hit.point);
    sample.radiance = intencityPerUnitSolidAngle / glm::distance2(position, hit.point);
    sample.visTester = VisibilityTester(hit.point, position);

    return sample;
}

glm::vec3 PointLight::TotalPower() const
{
    //Integral over sphere int(intensityConstant * dx)
    return 4 * glm::pi<float>() * intencityPerUnitSolidAngle;
}
