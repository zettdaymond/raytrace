#pragma once

#include "Light.h"
#include "Bounds.h"

class DirectionalLight
        : public Light
{
public:
    DirectionalLight(const glm::vec3 &aDirection, const glm::vec3 &aIntencityPerPoint);

    LightSample SampleLight(const HitRecord &hit) override;
    glm::vec3 TotalPower() const override;

    void Preprocess(const Scene& scene) override;

private:
    glm::vec3 direction;
    glm::vec3 intencityPerPoint;
    Bounds3f::BoundingSphere worldBoundsSphere;
};
