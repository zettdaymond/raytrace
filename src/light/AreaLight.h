#pragma once

#include <memory>

#include "Light.h"

class Shape;

class AreaLight
        : public Light
{
public:
    AreaLight(const Transform& aTransform, int nSamples);
    virtual glm::vec3 L() const = 0;
};



class DiffuseAreaLight
        : public AreaLight
{
public:
    DiffuseAreaLight(const Transform& aTransform, int nSamples);

    LightSample SampleLight(const HitRecord &hit) override;
    glm::vec3 TotalPower() const override;
    glm::vec3 L() const override;

private:
    std::shared_ptr<Shape> shape;
    glm::vec3 Lemit = glm::vec3(0.0f);
};






