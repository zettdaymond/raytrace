#pragma once

#include <memory>
#include <vector>

#include "glm/vec2.hpp"
#include "glm/vec3.hpp"
#include "glm/vec4.hpp"

#include "Texture.h"


class ImageTexture
        : public Texture
{
public:
    enum class eFormat
    {
        kRGB,
        kRGBA
    };

    enum class eWrap
    {
        kRepeat,
        kClamp,
        kBlack
    };

    enum class eInterpolation
    {
        kTrilinear
    };

    enum class eColorSpace
    {
        kLinear,
        ksRGB
    };

    static std::shared_ptr<ImageTexture> Create(const glm::ivec2& aResolution,
                                                const std::vector<unsigned char> &aData,
                                                eFormat aTextureFormat,
                                                eColorSpace aColorSpace,
                                                eWrap aWrapMode,
                                                eInterpolation aInterpolation);


    ImageTexture(const glm::ivec2& aResolution,
                 std::vector<glm::vec4> &&aData,
                 const eFormat aTextureFormat,
                 const eColorSpace aColorSpace,
                 const eWrap aWrapMode,
                 const eInterpolation aInterpolation);

    ~ImageTexture() = default;

    glm::vec4 GetValue(const HitRecord &hit) override;

private:
    struct MipMap
    {
        std::vector<glm::vec4> data;
        glm::ivec2 size;

        const glm::vec4& operator()(int i, int j) const
        {
            assert(j * size.x + i < data.size());
            return data[j * size.x + i];
        }

        glm::vec4& operator()(int i, int j)
        {
            assert(j * size.x + i < data.size());
            return data[j * size.x + i];
        }
    };

    using TextureData = std::vector<glm::vec4>;

    static std::vector<MipMap> GenerateMipMaps(const glm::ivec2& aResolution,
                                                    std::vector<glm::vec3> &&aData);

    glm::vec4 GetWrapedPixelColor(const TextureData& data, int s, int t);

    glm::ivec2 resolution;
    TextureData data;

    eFormat format;
    eColorSpace initialColorSpace;
    eWrap wrapMode;
    eInterpolation interpolation;

    //TODO
    std::vector<MipMap> pyramid;
};
