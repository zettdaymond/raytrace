#include "Texture.h"

#include "glm/vec3.hpp"

class ConstantTexture
        : public Texture
{
public:
    ConstantTexture(const glm::vec3& aColor)
        : color(aColor)
    {}

    glm::vec4 GetValue(const HitRecord &) override
    {
        return glm::vec4(color, 0.0f);
    }

    glm::vec3 GetColor() const
    {
        return color;
    }

    void SetColor(const glm::vec3 &value)
    {
        color = value;
    }

private:
    glm::vec3 color;
};
