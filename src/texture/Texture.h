#pragma once

#include "glm/vec4.hpp"

struct HitRecord;

class Texture
{
public:
    virtual ~Texture() = default;

    virtual glm::vec4 GetValue(const HitRecord& hit) = 0;
};


