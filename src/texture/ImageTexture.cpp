#include "ImageTexture.h"

#include "glm/exponential.hpp"
#include "glm/common.hpp"
#include "glm/gtc/integer.hpp"

#include "common/Utilities.h"
#include "shape/Shape.h"


namespace SImageTexture
{
    template<class T>
    void ConvertToLinearSpace(std::vector<T> &imageData)
    {
        static_assert (T::length() >= 3);

        for(auto& pixel : imageData)
        {
            pixel.r = InverseGammaCorrect(pixel.r);
            pixel.g = InverseGammaCorrect(pixel.g);
            pixel.b = InverseGammaCorrect(pixel.b);
        }
    }

    template<uint8_t components>
    auto TexToVec(const std::vector<unsigned char>& data, ImageTexture::eFormat format)
    {
        using storage = glm::vec<components, float, glm::defaultp>;
        std::vector<storage> out;
        out.reserve(data.size());

        const uint8_t stride = format == ImageTexture::eFormat::kRGBA
                                        ? 4
                                        : 3;

        assert(data.size() % stride == 0);

        for(std::size_t i = 0; i < data.size(); i += stride)
        {
            storage v(0.0f);

            for(uint32_t j = 0; j < glm::min(components, stride); j++)
            {
                v[j] = float(data[i + j]) / 255.f;
            }

            out.push_back( v );
        }

        return out;
    }

    template<class T>
    T RepeatModulo(T a, T b)
    {
        static_assert (std::is_integral_v<T>);
        T modulo = a % b;
        return modulo < 0 ? b + modulo : modulo;
    }
}

std::shared_ptr<ImageTexture> ImageTexture::Create(const glm::ivec2 &aResolution,
                                                   const std::vector<unsigned char>& aData,
                                                   eFormat aTextureFormat,
                                                   eColorSpace aColorSpace,
                                                   ImageTexture::eWrap aWrapMode,
                                                   ImageTexture::eInterpolation aInterpolation)
{
    if(!(IsPowerOf2(aResolution.x) && IsPowerOf2(aResolution.y)))
    {
        assert(false && "not supported");
    }

    auto data = SImageTexture::TexToVec<4>(aData, aTextureFormat);

    if(aColorSpace == eColorSpace::ksRGB)
    {
        SImageTexture::ConvertToLinearSpace(data);
    }

    return std::make_shared<ImageTexture>(aResolution,
                                          std::move(data),
                                          aTextureFormat,
                                          aColorSpace,
                                          aWrapMode,
                                          aInterpolation);
}

ImageTexture::ImageTexture(const glm::ivec2 &aResolution, std::vector<glm::vec4> &&aData, const ImageTexture::eFormat aTextureFormat, const ImageTexture::eColorSpace aColorSpace, const ImageTexture::eWrap aWrapMode, const ImageTexture::eInterpolation aInterpolation)
    : resolution(aResolution)
    , data(std::move(aData))
    , format(aTextureFormat)
    , initialColorSpace(aColorSpace) //TODO: Do not convert to vec4 at Create, Convert on demad (GetValue)
    , wrapMode(aWrapMode)
    , interpolation(aInterpolation)
{}

glm::vec4 ImageTexture::GetValue(const HitRecord &hit)
{
    const static auto kDefaultCyanColor = glm::vec4(0.0f, 1.0f, 1.0f, 1.0f);
    if(data.size() == 0)
    {
        return kDefaultCyanColor;
    }

    auto u = hit.uv.x;
    auto v = hit.uv.y;

    auto s = static_cast<int>((  u) * resolution.x);
    auto t = static_cast<int>((  v) * resolution.y);

    return GetWrapedPixelColor(data, s, t);
}

glm::vec4 ImageTexture::GetWrapedPixelColor(const ImageTexture::TextureData &data, int s, int t)
{
    switch(wrapMode)
    {
    case eWrap::kRepeat:
        s = SImageTexture::RepeatModulo(s, resolution.x);
        t = SImageTexture::RepeatModulo(t, resolution.y);
        break;
    case eWrap::kBlack:
        if(s < 0 || t < 0 || s > resolution.x || t > resolution.y)
        {
            static constexpr glm::vec4 kBlackColor(0.0f, 0.0f, 0.0f, 1.0f);
            return kBlackColor;
        }
        break;
    case eWrap::kClamp:
        s = glm::clamp(s, 0, resolution.x - 1);
        t = glm::clamp(t, 0, resolution.y - 1);
        break;
    default:
        assert(false && "Unsupported wrap");
        break;
    }

    const auto pixelIndex = t * resolution.x + s;
    assert(pixelIndex < data.size());

    return data[pixelIndex];
}

//std::vector<ImageTexture::MipMap> ImageTexture::GenerateMipMaps(const glm::ivec2 &aResolution, std::vector<glm::vec3>&& aData)
//{
//    std::vector<ImageTexture::MipMap> outPyramid;

//    int32_t totalLevels = 1 + glm::log2(glm::max(aResolution.x, aResolution.y));
//    outPyramid.resize(totalLevels);

//    outPyramid[0] = MipMap{std::move(aData), aResolution};

//    for(int i = 1; i < totalLevels; i++)
//    {
//        auto mipMapResolution = glm::max(glm::ivec2(1), outPyramid[i - 1].size / 2);
//        auto& mipMap = outPyramid[i];

//        mipMap.size = mipMapResolution;
//        mipMap.data.resize(mipMapResolution.x * mipMapResolution.y);

//        auto& previousMipMap = outPyramid[i - 1];

//        for(int u = 0; u < mipMapResolution.x; u++)
//        {
//            for(int v = 0; v < mipMapResolution.y; v++)
//            {
//                //average 2x2 grid
//                glm::vec3 average = previousMipMap(2*u, 2*v) +
//                        previousMipMap(2*u + 1, 2*v) +
//                        previousMipMap(2*u, 2*v + 1) +
//                        previousMipMap(2*u + 1, 2*v + 1);
//                average /= 4;
//                mipMap(u, v) = average;
//            }
//        }
//    }

//    return outPyramid;
//}
