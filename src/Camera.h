#pragma once

#include <memory>

#include "Ray.h"
#include "Bounds.h"
#include "Transform.h"

class Film;

class Camera
{
public:
    Camera(const std::shared_ptr<Film>& aFilm, const Bounds2f& aScreenSpaceWindow);

    glm::vec3 ToScreenSpace(const glm::vec3& rasterSpace) const;

    Bounds2f MakeDefaultScreenSpaceWindow() const;

    virtual Ray GetRay(const glm::vec2& pointOnFilm) const = 0;

    Transform GetTransform();

    void SetTransform(const Transform& tr);

    Film& GetFilm();

protected:
    std::shared_ptr<Film> film;
    Bounds2f screenSpaceWindow;

    Transform cameraToWorld; //local

    glm::mat4 cameraToScreen;
    glm::mat4 screenToCamera;

    Transform screenToRaster;
    Transform rasterToScreen;

    Transform rasterToCamera;
};


class OrthoCamera
        : public Camera
{
public:
    OrthoCamera(const std::shared_ptr<Film>& aFilm, float zNear, float zFar, float verticalSize);

    Ray GetRay(const glm::vec2& pointOnFilm) const override;

    Bounds2f MakeOrthoProjection(float verticalSize, glm::vec2 imageResolution) const;
};


class PerspectiveCamera
        : public Camera
{
public:
    PerspectiveCamera(const std::shared_ptr<Film>& aFilm, float aFieldOfView, float zNear, float zFar);
    Ray GetRay(const glm::vec2& pointOnFilm) const override;
    Bounds2f MakeScreenBounds(float aFieldOfView, float zNear, glm::vec2 imageResolution);
};
