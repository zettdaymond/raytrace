#pragma once

#include <memory>

#include "Bounds.h"
#include "shape/Shape.h"
#include "material/Material.h"

class Primitive
{
public:
    virtual ~Primitive() = default;

    virtual Bounds3f WorldBounds() const = 0;
    virtual std::optional<HitRecord> hit(const Ray& r, float t_min, float t_max) = 0;
    virtual std::optional<ScatterInfo> scatter(const Ray& ray, const HitRecord& hit) = 0;
    virtual Material* GetMaterial() = 0;
};


class GeometricPrimitive :
        public Primitive
{
public:
    GeometricPrimitive(const std::shared_ptr<Shape>& aShape,
                       const std::shared_ptr<Material>& aMaterial)
        : shape(aShape)
        , material(aMaterial)
    {}

    std::optional<HitRecord> hit(const Ray &r, float t_min, float t_max) override
    {
        if(shape)
        {
            if(auto hit = shape->hit(r, t_min, t_max))
            {
                hit->primitive = this;
                return hit;
            }
        }
        return {};
    }

    std::optional<ScatterInfo> scatter(const Ray &ray, const HitRecord &hit) override
    {
        if(material)
        {
            return material->scatter(ray, hit);
        }
        return {};
    }

    Material *GetMaterial() override
    {
        return material.get();
    }

    Bounds3f WorldBounds() const override
    {
        return shape->WorldBounds();
    }

private:
    std::shared_ptr<Shape> shape;
    std::shared_ptr<Material> material;
};








